=== WooCommerce Zoho Integration ===
Contributors: crmperks, sbazzi, asif876
Tags: woocommerce, zoho, woocommerce zoho, wooCommerce zoho integration, zoho and woocommerce, zoho crm woocommerce
Requires at least: 3.8
Tested up to: 5.1
Stable tag: 1.0.3
Version: 1.0.3
WC requires at least: 2.1
WC tested up to: 3.3.1
Requires PHP: 5.3
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

WooCommerce Zoho Plugin allows you to quickly integrate WooCommerce Orders with Zoho CRM.

== Description ==

Easily create leads, contacts, accounts, deals or any object in Zoho when an order is placed via WooCommerce. Learn more at [crmperks.com](https://www.crmperks.com/plugins/woocommerce-plugins/woocommerce-zoho-plugin/?utm_source=wordpress&amp;utm_medium=directory&amp;utm_campaign=readme)

== How to Setup ==

* Go to WooCommerce -> Settings -> Zoho tab then add new account.
* Go to WooCommerce -> Zoho Feeds tab then create new feed.
* Map required Zoho fields to WooCommerce Order fields.
* Send your test entry to Zoho CRM.
* Go to WooCommerce -> Zoho Logs and verify, if entry was sent to Zoho CRM.

**Connect Zoho Account**

Connect Zoho CRM Account to WooCommerce store by simply oauth 2.0 authentication. Also you can connect multiple Zoho accounts.

**Fields Mapping**

Simply Select Zoho Object then map WooCommerce Order fields to Zoho Object(Contact, Account, Lead, Deal, custom module etc) fields.

**Export Event**

Choose event, when WooCommerce Order data should be sent to Zoho CRM. For example , send WooCommerce Order to Zoho CRM on Order Completion.

**Primary Key**

Instead of creating new Object(Contact, Account, Lead, Deal, custom module etc) in zoho CRM, you can update old object by setting Primary Key field.

**Error Reporting**

If there is an error while sending data to Zoho CRM, an email containing the error details will be sent to the specified email address.

**CRM Logs**

Plugin saves detailed log of each WooCommerce Order whether sent (or not sent) to Zoho CRM and easily resend an Order to Zoho CRM.

**Export Logs as CSV**

Easily export Zoho Logs(WooCommerce Orders sent or not sent to Zoho) as a CSV file for bookkeeping, and accounting purposes.

**Full Synchronization**

All Woocommerce Orders are fully synchronized with Zoho. If you update/delete/restore an order that order will be updated/deleted/restored in Zoho CRM.

**Filter orders**

By default all Woocommerce orders are sent to Zoho CRM, but you can apply filters &amp; setup rules to limit the orders sent to Zoho CRM. For example sending Orders from specific city to Zoho.

**Send Data As Notes**

Send one to many WooCommerce Order fields data as an object(Contact, Account, Lead, Deal, custom module etc) Note in zoho CRM.

**Assign Objects**

An Object(Contact, Account, Lead, Deal, custom module etc) created/updated by one feed can be assigned to the Object created/updated by other feed.


<blockquote><strong>Premium Version.</strong>

Following features are available in Premium version only.<a href="https://www.crmperks.com/plugins/woocommerce-plugins/woocommerce-zoho-plugin/?utm_source=wordpress&amp;utm_medium=directory&amp;utm_campaign=zoho_readme">Upgrade to Pro version</a>

<ul>
 	<li>Add WooCommerce Order Items to Zoho.</li>
 	<li>Zoho Custom fields.</li>
 	<li>Zoho Phone Number fields.</li>
 	<li>Select Zoho Object Layout.</li>
 	<li>Add a lead to campaign in Zoho CRM.</li>
 	<li>Assign owner to any object(Contact, lead , account etc) in Zoho CRM.</li>
 	<li>Assign object created/updated/found by one feed to other feed. For example assigning a contact to a custom Zoho object.</li>
 	<li>Track Google Analytics Parameters and Geolocation of a WooCommerce customer.</li>
 	<li>Lookup lead's email and phone number using popular email and phone lookup services.</li>
</ul>
</blockquote>

== Premium Addons ==

We have 20+ premium addons and new ones being added regularly, it's likely we have everything you'll ever need.[View All Add-ons](https://www.crmperks.com/add-ons/?utm_medium=referral&amp;utm_source=wordpress&amp;utm_campaign=WC+zoho+Readme&amp;utm_content=WC)

== Want to send data to other crm ==
We have Premium Extensions for 20+ CRMs.[View All CRM Extensions](https://www.crmperks.com/plugin-category/woocommerce-plugins/?utm_source=wordpress&amp;utm_medium=directory&amp;utm_campaign=zoho_readme)

== Screenshots ==

1. Connect Zoho CRM Account.
2. Map Zoho CRM Fields to WooCommerce Order fields.
3. Zoho CRM logs.
4. Send WooCommerce Order to Zoho CRM.
5. Get Customer's email infomation from Full Contact(Premium feature).
6. Get Customer's geolocation, browser and OS (Premium feature).

== Frequently Asked Questions ==

= Where can I get support? =

Our team provides free support at <a href="https://www.crmperks.com/contact-us/">https://www.crmperks.com/contact-us/</a>.

= WooCommerce Zoho integration =

Easily integrate WooCommerce Orders to Zoho CRM with free WooCommerce Zoho Plugin. Connect Zoho account and map WooCommerce Orders fields to Zoho object fields. You can integrate WooCommerce order to Zoho CRM Contact, Lead, Account, Deal or any custom module.

= woocommerce and zoho connector =

Easily connect woocommerce to zoho crm with this free woocommerce and zoho connector. If you need all features then use woocommerce and zoho crm connector pro which comes with lot of additional features.

= zoho and woocommerce =

Woocomerce is free and open source ecommerce plugin and basic features of Zoho crm are free. You can easily connect zoho and woocommerce with woocommerce and zoho crm connector pro.

= How zoho and woocommerce integration works =

This free zoho crm woocommerce plugin can create/update any object(contact,account,lead, product etc) in Zoho crm when an Order is placed in Woocommerce.

== Changelog ==

= 1.0.3 =
* Fixed "check filter" feature.

= 1.0.2 =
* Added support for multi-lookup field.

= 1.0.1 =
* Moved to Zoho API V2.
* Added support for Zoho Eu datacenter.

= 1.0.0 =
* Initial release.

== Upgrade Notice ==

= 1.0.1 =
1.0.1 is a major update. You will have to re-configure Zoho accounts and feeds after upgrading from old versions

= 1.0 =
1.0.1 is a major update. You will have to re-configure Zoho accounts and feeds after upgrading from old versions
<?php
if ( ! defined( 'ABSPATH' ) ) {
     exit;
 }                                            
 ?>  <h3><?php _e('Uninstall WooCommerce Zoho Plugin','woocommerce-zoho-crm'); ?></h3>
  <?php
  if(isset($_POST[$this->id.'_uninstall'])){ 
  ?>
  <div class="vxc_alert updated  below-h2">
  <h3><?php _e('Success','woocommerce-zoho-crm'); ?></h3>
  <p><?php _e('WooCommerce Zoho Plugin has been successfully uninstalled','woocommerce-zoho-crm'); ?></p>
  <p>
  <a class="button button-hero button-primary" href="plugins.php"><?php _e("Go to Plugins Page",'woocommerce-zoho-crm'); ?></a>
  </p>
  </div>
  <?php
  }else{
  ?>
  <div class="vxc_alert error below-h2">
  <h3><?php _e("Warning",'woocommerce-zoho-crm'); ?></h3>
  <p><?php _e('This Operation will delete all Zoho logs and feeds.','woocommerce-zoho-crm'); ?></p>
  <p><button class="button button-hero button-secondary" id="vx_uninstall" type="submit" onclick="return confirm('<?php _e("Warning! ALL Zoho Feeds and Logs will be deleted. This cannot be undone. OK to delete, Cancel to stop.", 'woocommerce-zoho-crm')?>');" name="<?php echo $this->id ?>_uninstall" title="<?php _e("Uninstall",'woocommerce-zoho-crm'); ?>" value="yes"><?php _e("Uninstall",'woocommerce-zoho-crm'); ?></button></p>
  </div>
  <?php
  } ?>
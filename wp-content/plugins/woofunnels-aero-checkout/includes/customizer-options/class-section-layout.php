<?php
defined( 'ABSPATH' ) || exit;

class WFACP_Sectionlayout {

	public static $customizer_key_prefix = 'wfacp_';
	public static $_instance = null;
	/**
	 * @var WFACP_Template_Common
	 */
	private $template_common;

	/**
	 * WFACP_SectionCustomerCare constructor.
	 *
	 * @param null|WFACP_Template_Common $template_common
	 */
	protected function __construct( $template_common = null ) {
		if ( ! is_null( $template_common ) ) {
			$this->template_common = $template_common;
		}
	}

	public static function get_instance( $template_common ) {
		if ( self::$_instance == null ) {
			self::$_instance = new self( $template_common );
		}

		return self::$_instance;
	}

	public function layout_settings() {

		$selected_template_slug = $this->template_common->get_template_slug();

		/** PANEL: Layout Setting */
		$layout_panel = array();

		$layout_panel['wfacp_layout'] = array(
			'panel'    => 'no',
			'data'     => array(
				'priority'    => 10,
				'title'       => __( 'Widget Visibility', 'woofunnels-aero-checkout' ),
				'description' => '',
			),
			'sections' => array(
				'section' => array(
					'data'   => array(
						'title'    => __( 'Widget Visibility', 'woofunnels-aero-checkout' ),
						'priority' => 10,
					),
					'fields' => array(
						'ct_components' => array(
							'type'     => 'custom',
							'default'  => '<div class="options-title-divider">' . esc_html__( 'Sections', 'woofunnels-aero-checkout' ) . '</div>',
							'priority' => 40,
						),

						$selected_template_slug . '_sidebar_layout_order'       => array(
							'type'        => 'sortable',
							'label'       => __( 'Elements Order & Visibility for Desktop View Sidebar', 'woofunnels-aero-checkout' ),
							'description' => __( 'Drag and Drop Sections to modify its position. <br>Click on Eye icon to turn ON/OFF visibility of the section.', 'woofunnels-aero-checkout' ),
							'default'     => array(
								'wfacp_benefits_0',
								'wfacp_testimonials_0',
								'wfacp_promises_0',
								'wfacp_assurance_0',
								'wfacp_customer_0',
							),
							'choices'     => array(
								'wfacp_benefits_0'     => esc_attr__( 'Benefits', 'woofunnels-aero-checkout' ),
								'wfacp_testimonials_0' => esc_attr__( 'Testimonials', 'woofunnels-aero-checkout' ),
								'wfacp_promises_0'     => esc_attr__( 'Promises', 'woofunnels-aero-checkout' ),
								'wfacp_assurance_0'    => esc_attr__( 'Assurance', 'woofunnels-aero-checkout' ),
								'wfacp_customer_0'     => esc_attr__( 'Customer Support', 'woofunnels-aero-checkout' ),

							),
							'priority'    => 50,
						),
						$selected_template_slug . '_mobile_sections_page_order' => array(
							'type'        => 'sortable',
							'label'       => __( 'Elements Order & Visibility for Mobile View', 'woofunnels-aero-checkout' ),
							'description' => __( '<i>Tip: You can rearrange widgets or control visibility on mobile. Shrinking the browser and checking preview may not lead to real results. Check on mobile decide to see the actual preview.</i>', 'woofunnels-aero-checkout' ),
							'default'     => array(
								'wfacp_form',
								'wfacp_product',
								'wfacp_benefits_0',
								'wfacp_testimonials_0',
								'wfacp_promises_0',
								'wfacp_assurance_0',
								'wfacp_customer_0',
							),
							'choices'     => array(
								'wfacp_form'           => esc_attr__( 'Form', 'woofunnels-aero-checkout' ),
								'wfacp_product'        => esc_attr__( 'Product', 'woofunnels-aero-checkout' ),
								'wfacp_benefits_0'     => esc_attr__( 'Benefits', 'woofunnels-aero-checkout' ),
								'wfacp_testimonials_0' => esc_attr__( 'Testimonials', 'woofunnels-aero-checkout' ),
								'wfacp_promises_0'     => esc_attr__( 'Promises', 'woofunnels-aero-checkout' ),
								'wfacp_assurance_0'    => esc_attr__( 'Assurance', 'woofunnels-aero-checkout' ),
								'wfacp_customer_0'     => esc_attr__( 'Customer Support', 'woofunnels-aero-checkout' ),

							),
							'priority'    => 55,
						),

						'hidden_feilds'   => array(
							'type'     => 'hidden',
							'label'    => '',
							'default'  => 1,
							'priority' => 55,

						),
						'customizer_data' => array(
							'type'            => 'sortable',
							'label'           => __( 'All Customizer Data', 'woofunnels-aero-checkout' ),
							'description'     => __( 'Drag and Drop Sections to modify its position. <br>Click on Eye icon to turn ON/OFF visibility of the section.', 'woofunnels-aero-checkout' ),
							'default'         => array(
								'wfacp_form',
								'wfacp_product',
								'wfacp_benefits_0',
								'wfacp_testimonials_0',
								'wfacp_promises_0',
								'wfacp_assurance_0',
								'wfacp_customer_0',
							),
							'choices'         => array(
								'wfacp_form'           => esc_attr__( 'Form', 'woofunnels-aero-checkout' ),
								'wfacp_product'        => esc_attr__( 'Product', 'woofunnels-aero-checkout' ),
								'wfacp_benefits_0'     => esc_attr__( 'Benefits', 'woofunnels-aero-checkout' ),
								'wfacp_testimonials_0' => esc_attr__( 'Testimonials', 'woofunnels-aero-checkout' ),
								'wfacp_promises_0'     => esc_attr__( 'Promises', 'woofunnels-aero-checkout' ),
								'wfacp_assurance_0'    => esc_attr__( 'Assurance', 'woofunnels-aero-checkout' ),
								'wfacp_customer_0'     => esc_attr__( 'Customer Support', 'woofunnels-aero-checkout' ),

							),
							'priority'        => 55,
							'active_callback' => array(
								array(
									'setting'  => 'wfacp_layout_section_hidden_feilds',
									'operator' => '!=',
									'value'    => '1',
								),
							),
						),
					),
				),
			),
		);

		$layout_panel['wfacp_layout'] = apply_filters( 'wfacp_customizer_layout', $layout_panel['wfacp_layout'], 'wfacp_layout' );

		return $layout_panel;

	}


}

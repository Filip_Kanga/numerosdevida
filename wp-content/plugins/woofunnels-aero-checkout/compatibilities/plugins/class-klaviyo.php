<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


class WFACP_Compatibility_With_Klaviyo {
	public function __construct() {
		add_filter( 'wfacp_advanced_fields', [ $this, 'add_fields' ] );
		add_action( 'wfacp_after_checkout_page_found', [ $this, 'actions' ] );
	}

	public function actions() {
		add_filter( 'woocommerce_form_field_args', [ $this, 'add_default_wfacp_styling' ], 10, 2 );
	}

	public function add_fields( $field ) {
		if ( $this->is_enable() ) {
			$field['kl_newsletter_checkbox'] = [
				'type'          => 'checkbox',
				'default'       => true,
				'label'         => __( 'Klaviyo', 'woocommerce-klaviyo' ),
				'validate'      => [],
				'id'            => 'kl_newsletter_checkbox',
				'required'      => false,
				'wrapper_class' => [],
				'class'         => [ 'kl_newsletter_checkbox_field' ],
			];

		}

		return $field;
	}

	public function is_enable() {
		if ( class_exists( 'WooCommerceKlaviyo' ) ) {
			return true;
		}

		return false;
	}

	public function add_default_wfacp_styling( $args, $key ) {
		if ( $key == 'kl_newsletter_checkbox' && $this->is_enable() ) {
			$klaviyo_settings = get_option( 'klaviyo_settings' );
			if ( isset( $klaviyo_settings['klaviyo_newsletter_text'] ) && '' !== $klaviyo_settings['klaviyo_newsletter_text'] ) {
				$args['label'] = $klaviyo_settings['klaviyo_newsletter_text'];
			}
		}

		return $args;
	}


}

WFACP_Plugin_Compatibilities::register( new WFACP_Compatibility_With_Klaviyo(), 'klaviyo' );

<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


class WFACP_Compatibility_With_WECFB {

	public $user_id = 0;
	private $fields_added = false;
	private $shipping_fields_added = false;
	private $configs = [];

	public function __construct() {

		add_action( 'wfacp_after_checkout_page_found', [ $this, 'actions' ] );
		add_filter( 'wfacp_form_section', [ $this, 'checkout_billing_sections' ] );
		add_filter( 'wfacp_form_section', [ $this, 'checkout_shipping_sections' ], 12 );
	}

	public function actions() {
		add_action( 'wp_footer', [ $this, 'add_js' ] );
	}

	public function checkout_shipping_sections( $sections ) {

		if ( $this->shipping_fields_added ) {

			return $sections;
		}
		if ( count( $sections ) == 0 ) {
			return $sections;
		}
		if ( ! class_exists( 'Extra_Checkout_Fields_For_Brazil_Front_End' ) || ! class_exists( 'Extra_Checkout_Fields_For_Brazil_Front_End' ) ) {
			return $sections;
		}

		if ( isset( $sections['fields']['wfacp_end_divider_shipping'] ) ) {
			try {

				$this->shipping_fields_added = true;

				$end_address_found     = false;
				$end_address_closser   = $sections['fields']['wfacp_end_divider_shipping'];
				$after_address_element = [];
				$is_hidedable          = false;

				foreach ( $sections['fields'] as $index => $field ) {
					if ( $end_address_found ) {
						$after_address_element[] = $field;
						unset( $sections['fields'][ $index ] );
					}
					if ( isset( $field['class'] ) && in_array( 'wfacp_shipping_fields', $field['class'] ) ) {
						$is_hidedable = true;
					}
					if ( 'wfacp_end_divider_shipping' === $index ) {
						unset( $sections['fields'][ $index ] );
						$end_address_found = true;
					}
				}

				if ( false == $end_address_found ) {
					return $sections;
				}

				$new_fields = array();

				$new_fields['shipping_number'] = [
					'label'    => __( 'Number', 'woocommerce-extra-checkout-fields-for-brazil' ),
					'class'    => [ 'form-row-first', 'address-field', 'wfacp-form-control-wrapper', 'wfacp-col-left-half' ],
					'clear'    => true,
					'required' => true,
					'priority' => 55,
				];

				$new_fields['shipping_neighborhood'] = [
					'label' => __( 'Neighborhood', 'woocommerce-extra-checkout-fields-for-brazil' ),
					'class' => [ 'form-row-first', 'address-field', 'wfacp-form-control-wrapper', 'wfacp-col-right-half' ],
					'clear' => true,
				];

				if ( is_array( $new_fields ) && count( $new_fields ) > 0 ) {
					foreach ( $new_fields as $fkey => $fvalue ) {
						$fvalue['id'] = $fkey;

						if ( $is_hidedable ) {
							$fvalue['class'][] = 'wfacp_shipping_fields';
							$fvalue['class'][] = 'wfacp_shipping_field_hide';
						}

						$sections['fields'][] = $fvalue;
					}
				}

				$sections['fields']['wfacp_end_divider_shipping'] = $end_address_closser;

				if ( count( $after_address_element ) > 0 ) {
					$last_field_type = '';
					foreach ( $after_address_element as $element ) {
						if ( isset( $element['type'] ) && $element['type'] === 'wfacp_start_divider' ) {
							if ( false !== strpos( $element['id'], '_shipping' ) ) {
								$last_field_type            = 'shipping';
								$tid                        = 'wfacp_start_divider_shipping';
								$sections['fields'][ $tid ] = WFACP_Common::get_start_divider_field( 'shipping' );
							} elseif ( false !== strpos( $element['id'], '_billing' ) ) {
								$last_field_type            = 'billing';
								$tid                        = 'wfacp_start_divider_billing';
								$sections['fields'][ $tid ] = WFACP_Common::get_start_divider_field( 'billing' );
							}
						} elseif ( isset( $element['type'] ) && $element['type'] === 'wfacp_end_divider' ) {
							$tid                        = 'wfacp_end_divider_' . $last_field_type;
							$sections['fields'][ $tid ] = WFACP_Common::get_end_divider_field();
						} else {
							$sections['fields'][] = $element;
						}
					}
				}
			} catch ( Exception $e ) {

			}
		}

		return $sections;
	}

	public function checkout_billing_sections( $sections ) {

		if ( $this->fields_added ) {
			return $sections;
		}
		if ( count( $sections ) == 0 ) {
			return $sections;
		}
		if ( ! class_exists( 'Extra_Checkout_Fields_For_Brazil_Front_End' ) || ! class_exists( 'Extra_Checkout_Fields_For_Brazil_Front_End' ) ) {
			return $sections;
		}

		if ( isset( $sections['fields']['wfacp_end_divider_billing'] ) ) {
			try {

				$this->fields_added = true;

				$end_address_found     = false;
				$end_address_closser   = $sections['fields']['wfacp_end_divider_billing'];
				$after_address_element = [];
				$is_hidedable          = false;
				foreach ( $sections['fields'] as $index => $field ) {
					if ( $end_address_found ) {
						$after_address_element[] = $field;
						unset( $sections['fields'][ $index ] );
					}
					if ( isset( $field['class'] ) && in_array( 'wfacp_billing_fields', $field['class'] ) ) {
						$is_hidedable = true;
					}
					if ( 'wfacp_end_divider_billing' === $index ) {
						unset( $sections['fields'][ $index ] );
						$end_address_found = true;
					}
				}

				if ( false == $end_address_found ) {
					return $sections;
				}

				$new_fields = array();

				// Get plugin settings.
				$settings    = get_option( 'wcbcf_settings' );
				$person_type = intval( $settings['person_type'] );

				if ( 0 !== $person_type ) {
					if ( 1 === $person_type ) {
						$new_fields['billing_persontype'] = [
							'type'        => 'select',
							'label'       => __( 'Person type', 'woocommerce-extra-checkout-fields-for-brazil' ),
							'class'       => [ 'form-row-wide', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-full' ],
							'input_class' => [ 'wc-ecfb-select' ],
							'required'    => false,
							'options'     => [
								'1' => __( 'Individuals', 'woocommerce-extra-checkout-fields-for-brazil' ),
								'2' => __( 'Legal Person', 'woocommerce-extra-checkout-fields-for-brazil' ),
							],
							'priority'    => 22,
						];
					}

					if ( 1 === $person_type || 2 === $person_type ) {
						if ( isset( $settings['rg'] ) ) {
							$new_fields['billing_cpf'] = [
								'label'    => __( 'CPF', 'woocommerce-extra-checkout-fields-for-brazil' ),
								'class'    => [ 'form-row-first', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-left-half' ],
								'required' => false,
								'type'     => 'tel',
								'priority' => 23,
							];

							$new_fields['billing_rg'] = [
								'label'    => __( 'RG', 'woocommerce-extra-checkout-fields-for-brazil' ),
								'class'    => [ 'form-row-last', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-right-half' ],
								'required' => false,
								'priority' => 24,
							];
						} else {
							$new_fields['billing_cpf'] = [
								'label'    => __( 'CPF', 'woocommerce-extra-checkout-fields-for-brazil' ),
								'class'    => [ 'form-row-wide', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-full' ],
								'required' => false,
								'type'     => 'tel',
								'priority' => 23,
							];
						}
					}

					if ( 1 === $person_type || 3 === $person_type ) {

						if ( isset( $settings['ie'] ) ) {
							$new_fields['billing_cnpj'] = [
								'label'    => __( 'CNPJ', 'woocommerce-extra-checkout-fields-for-brazil' ),
								'class'    => [ 'form-row-first', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-left-half' ],
								'required' => false,
								'type'     => 'tel',
								'priority' => 26,
							];

							$new_fields['billing_ie'] = [
								'label'    => __( 'State Registration', 'woocommerce-extra-checkout-fields-for-brazil' ),
								'class'    => [ 'form-row-last', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-right-half' ],
								'required' => false,
								'priority' => 27,
							];
						} else {
							$new_fields['billing_cnpj'] = [
								'label'    => __( 'CNPJ', 'woocommerce-extra-checkout-fields-for-brazil' ),
								'class'    => [ 'form-row-wide', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-full' ],
								'required' => false,
								'type'     => 'tel',
								'priority' => 26,
							];
						}
					}
				}

				if ( isset( $settings['birthdate_sex'] ) ) {
					$new_fields['billing_birthdate'] = [
						'label'    => __( 'Birthdate', 'woocommerce-extra-checkout-fields-for-brazil' ),
						'class'    => [ 'form-row-first', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-left-half' ],
						'clear'    => false,
						'required' => true,
						'priority' => 31,
					];

					$new_fields['billing_sex'] = [
						'type'        => 'select',
						'label'       => __( 'Sex', 'woocommerce-extra-checkout-fields-for-brazil' ),
						'class'       => [ 'form-row-last', 'person-type-field', 'wfacp-form-control-wrapper', 'wfacp-col-right-half' ],
						'input_class' => [ 'wc-ecfb-select' ],
						'clear'       => true,
						'required'    => true,
						'options'     => [
							''                                                             => __( 'Select', 'woocommerce-extra-checkout-fields-for-brazil' ),
							__( 'Female', 'woocommerce-extra-checkout-fields-for-brazil' ) => __( 'Female', 'woocommerce-extra-checkout-fields-for-brazil' ),
							__( 'Male', 'woocommerce-extra-checkout-fields-for-brazil' )   => __( 'Male', 'woocommerce-extra-checkout-fields-for-brazil' ),
						],
						'priority'    => 32,
					];
				}

				$new_fields['billing_number'] = array(
					'label'    => __( 'Number', 'woocommerce-extra-checkout-fields-for-brazil' ),
					'class'    => [ 'form-row-first', 'address-field', 'wfacp-form-control-wrapper', 'wfacp-col-left-half' ],
					'clear'    => true,
					'required' => true,
					'priority' => 55,
				);

				$new_fields['billing_neighborhood'] = array(
					'label'    => __( 'Neighborhood', 'woocommerce-extra-checkout-fields-for-brazil' ),
					'class'    => [ 'form-row-first', 'address-field', 'wfacp-form-control-wrapper', 'wfacp-col-right-half' ],
					'clear'    => true,
					'priority' => 65,
				);

				if ( isset( $settings['cell_phone'] ) ) {

					$new_fields['billing_cellphone'] = array(
						'label'    => __( 'Cell Phone', 'woocommerce-extra-checkout-fields-for-brazil' ),
						'class'    => [ 'form-row-last', 'wfacp-form-control-wrapper', 'wfacp-col-full' ],
						'clear'    => true,
						'priority' => 105,
					);

				}

				if ( is_array( $new_fields ) && count( $new_fields ) > 0 ) {
					foreach ( $new_fields as $fkey => $fvalue ) {
						$fvalue['id'] = $fkey;

						if ( $is_hidedable ) {
							$fvalue['class'][] = 'wfacp_billing_fields';
							$fvalue['class'][] = 'wfacp_billing_field_hide';
						}

						$sections['fields'][] = $fvalue;
					}
				}

				$sections['fields']['wfacp_end_divider_billing'] = $end_address_closser;
				if ( count( $after_address_element ) > 0 ) {

					$last_field_type = '';
					foreach ( $after_address_element as $element ) {

						if ( $element['type'] === 'wfacp_start_divider' ) {
							if ( false !== strpos( $element['id'], '_shipping' ) ) {
								$last_field_type            = 'shipping';
								$tid                        = 'wfacp_start_divider_shipping';
								$sections['fields'][ $tid ] = WFACP_Common::get_start_divider_field( 'shipping' );
							} elseif ( false !== strpos( $element['id'], '_billing' ) ) {
								$last_field_type            = 'billing';
								$tid                        = 'wfacp_start_divider_billing';
								$sections['fields'][ $tid ] = WFACP_Common::get_start_divider_field( 'billing' );
							}
						} elseif ( $element['type'] === 'wfacp_end_divider' ) {
							$tid                        = 'wfacp_end_divider_' . $last_field_type;
							$sections['fields'][ $tid ] = WFACP_Common::get_end_divider_field();
						} else {
							$sections['fields'][] = $element;
						}
					}
				}
			} catch ( Exception $e ) {

			}
		}

		return $sections;
	}


	public function add_js() {

		if ( ! $this->fields_added ) {
			return;
		}
		?>
        <script>
            window.addEventListener('load', function () {
                (function ($) {
                    $(document.body).on('wfacp_step_switching', function () {
                        /*
						add select2 js for extra brazil field
						 */
                        let wc_ecfb_select = $('.wc-ecfb-select');
                        if ($().select2 && wc_ecfb_select.length > 0) {
                            wc_ecfb_select.select2('destroy');
                            setTimeout(function () {
                                $('.wc-ecfb-select').select2();
                            }, 50);
                        }
                    });
                    $(document).on('change', '#billing_same_as_shipping', function () {
                        if ($(this).is(":checked")) {
                            let wc_ecfb_select = $('.wc-ecfb-select');
                            if ($().select2 && wc_ecfb_select.length > 0) {
                                wc_ecfb_select.select2('destroy');
                                setTimeout(function () {
                                    $('.wc-ecfb-select').select2();
                                }, 50);
                            }
                        }
                    });
                })(jQuery);
            });
        </script>
		<?php

	}


}

WFACP_Plugin_Compatibilities::register( new WFACP_Compatibility_With_WECFB(), 'wfcfb' );

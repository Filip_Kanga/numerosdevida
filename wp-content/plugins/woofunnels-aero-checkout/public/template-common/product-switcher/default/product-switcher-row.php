<?php
defined( 'ABSPATH' ) || exit;
/**
 * @var $pro WC_Product;
 */
if ( ! $pro instanceof WC_Product ) {
	return;
}
//echo $cart_item_key;
if ( isset( WC()->cart->removed_cart_contents[ $cart_item_key ] ) ) {
	return;
}

// check if product is not added in cart then we check status of product
if ( '' == $cart_item_key ) {
	$manage_stock = WFACP_Common::check_manage_stock( $pro, $product_data['org_quantity'] );
	if ( false == $manage_stock || false == $pro->is_purchasable() ) {
		return;
	}
}

$hide_quantity_switcher = wc_string_to_bool( $switcher_settings['settings']['hide_quantity_switcher'] );
$hide_quick_view        = wc_string_to_bool( $switcher_settings['settings']['hide_quick_view'] );
$hide_quantity_switcher = wc_string_to_bool( $switcher_settings['settings']['hide_quantity_switcher'] );
$enable_delete_item     = false;
$you_save_text          = isset( $product_data['you_save_text'] ) ? $product_data['you_save_text'] : '';
$input_class            = 'wfacp_product_choosen';
if ( 'radio' === $type ) {
	$input_class = 'wfacp_product_switch';
} elseif ( 'hidden' == $type ) {
	$enable_delete_item = wc_string_to_bool( $switcher_settings['settings']['enable_delete_item'] );
}
$forceAllSetting = false;
if ( isset( $switcher_settings['product_settings']['add_to_cart_setting'] ) && 1 == $switcher_settings['product_settings']['add_to_cart_setting'] ) {
	$forceAllSetting = true;

}
$cart_variation_id = 0;
if ( ! is_null( $cart_item ) ) {
	if ( isset( $cart_item['variation_id'] ) ) {
		$cart_variation_id = $cart_item['variation_id'];
	}
}
$product_is_hide_cls = 'wfacp_without_qty';
if ( true != $hide_quantity_switcher ) {
	$product_is_hide_cls = 'wfacp_with_qty';
}
$variation_attributes = [];
$product_attributes   = [];
global $wfacp_products_attributes_data;
if ( ! is_null( $cart_item ) && isset( $cart_item['variation_id'] ) ) {
	if ( is_array( $cart_item['variation'] ) && count( $cart_item['variation'] ) ) {
		$product_attributes = $cart_item['variation'];
	} elseif ( 'variation' == $cart_item['data']->get_type() ) {
		$product_attributes = $cart_item['data']->get_attributes();
	}
} elseif ( 'variation' == $pro->get_type() ) {
	$product_attributes = $pro->get_attributes();
}
$is_variation_error = '';
$attributes_keys    = [];
if ( count( $product_attributes ) > 0 ) {
	$wfacp_products_attributes_data[ $pro->get_id() ]['attributes'] = $product_attributes;
	foreach ( $product_attributes as $a_key => $a_val ) {
		if ( '' != $a_val ) {
			$variation_attributes[] = $a_val;
		} else {
			if ( in_array( $product_data['type'], WFACP_Common::get_variable_product_type() ) && $cart_variation_id > 0 ) {
				$is_variation_error = 'wfacp_incomplete_variation';
				$attributes_keys[]  = ucwords( str_replace( [ 'attribute_', 'attribute_pa_' ], '', $a_key ) );
			}
		}
	}
}
$checked_cls = 'wfacp_ps_checked';
if ( '' == $product_data['is_checked'] ) {
	$checked_cls = 'wfacp_ps_not_checked';
}
$ps_cls     = 'ps_' . $type;
$cart_count = count( WC()->cart->get_cart_contents() );

$product_selected_class = '';
if ( ! isset( WC()->cart->removed_cart_contents[ $cart_item_key ] ) ) {
	if ( 'radio' === $type && '' !== $product_data['is_checked'] && '' !== $cart_item_key ) {
		$product_selected_class = 'wfacp-selected-product';
	} else {
		if ( '' !== $cart_item_key ) {
			$product_selected_class = 'wfacp-selected-product';
		}
	}
}
$is_sold_individually = false;
$item_key             = isset( $product_data['item_key'] ) ? $product_data['item_key'] : '';
if ( isset( $switcher_settings['products'][ $item_key ] ) ) {
	$title     = $switcher_settings['products'][ $item_key ]['title'];
	$old_title = $switcher_settings['products'][ $item_key ]['old_title'];
	if ( '' !== $title && $title !== $old_title ) {
		if ( in_array( $pro->get_type(), WFACP_Common::get_variation_product_type() ) ) {
			$product_title = $title . '-' . implode( ',', $variation_attributes ) . ' ';
		} else {
			$product_title = $title;
		}
	} else {
		if ( in_array( $pro->get_type(), WFACP_Common::get_variation_product_type() ) ) {
			$product_title = $pro->get_name();
		} else {
			$product_title = $pro->get_title();
		}
	}
} else {
	if ( in_array( $pro->get_type(), WFACP_Common::get_variation_product_type() ) ) {
		$product_title = $pro->get_name();
	} else {
		$product_title = $pro->get_title();
	}
}
$best_value          = isset( $product_data['best_value_text'] ) ? $product_data['best_value_text'] : '';
$best_value_position = isset( $product_data['best_value_position'] ) ? $product_data['best_value_position'] : null;
$quick_preview       = '';
$quick_preview       = '';
$eye_icon_url        = WFACP_PLUGIN_URL . '/assets/img/show_popup.svg';
$choose_label        = '';


if ( in_array( $product_data['type'], WFACP_Common::get_variable_product_type() ) ) {
	/**
	 * @var $pro WC_Product_Variable;
	 */
	$choose_label = sprintf( "<a href='#' class='wfacp_qv-button var_product' qv-id='%d' qv-var-id='%d'>%s</a>", $product_data['id'], $cart_variation_id, apply_filters( 'wfacp_choose_option_text', __( 'Choose an option', 'woocommerce' ) ) );
	if ( true != $hide_quick_view ) {
		$quick_preview = sprintf( "<a class='wfacp_qv-button' qv-id='%d'  qv-var-id='%d'><img src='%s'></a>", $product_data['id'], $cart_variation_id, $eye_icon_url );
	}
} else {


	if ( true != $hide_quick_view ) {
		$quick_preview = sprintf( "<a class='wfacp_qv-button' qv-id='%d'><img src='%s'></a>", $pro->get_id(), $eye_icon_url );
	}
}

$you_save_text_html = '';
if ( '' !== $you_save_text ) {
	$subscription_tryl   = 0;
	$subscription_signup = 0;
	if ( in_array( $pro->get_type(), WFACP_Common::get_subscription_product_type() ) ) {
		$subscription_tryl   = WC_Subscriptions_Product::get_trial_length( $pro );
		$subscription_signup = WC_Subscriptions_Product::get_sign_up_fee( $pro );
	}

	$have_saving_value_merge_tag      = strpos( $you_save_text, '{{saving_value}}' );
	$have_saving_percentage_merge_tag = strpos( $you_save_text, '{{saving_percentage}}' );
	if ( ( false !== $have_saving_value_merge_tag || false !== $have_saving_percentage_merge_tag ) ) {
		if ( $subscription_tryl > 0 || $subscription_signup > 0 ) {
			//available  for future updates
		} else {
			$save_html = WFACP_Common::product_switcher_merge_tags( $you_save_text, $price_data, $pro, $product_data, $cart_item, $cart_item_key );
			if ( '' !== $save_html ) {
				$you_save_text_html = sprintf( '<div class="wfacp_you_save_text">%s</div>', $save_html );
			}
		}
	} else {
		// do not have merge tag Or Static you save text
		$save_html = WFACP_Common::product_switcher_merge_tags( $you_save_text, $price_data, $pro, $product_data, $cart_item, $cart_item_key );
		if ( '' !== $save_html ) {
			$you_save_text_html = sprintf( '<div class="wfacp_you_save_text">%s</div>', $save_html );
		}
	}
}
$subscription_product_string = '';
if ( in_array( $pro->get_type(), WFACP_Common::get_subscription_product_type() ) ) {
	$subscription_product_string = sprintf( "<div class='wfacp_product_subs_details'>%s</div>", WFACP_Common::subscription_product_string( $pro, $product_data, $cart_item, $cart_item_key ) );
}

if ( $pro->is_sold_individually() ) {
	$is_sold_individually = true;

}
$best_val_class = '';
if ( '' != $best_value ) {
	$best_val_class = 'wfacp_best_val_wrap';
}


$enable_delete_options = WFACP_Common::delete_option_enable_in_product_switcher();
if ( false == $enable_delete_options ) {
	$product_data['enable_delete'] = true;
}
$enable_hide_img = '';
if ( ( isset( $product_data['hide_product_image'] ) && true === $product_data['hide_product_image'] ) && true === $forceAllSetting ) {
	$enable_hide_img = 'wfacp_ps_enable_hideImg1';
} else {
	$enable_hide_img = 'wfacp_ps_disable_hideImg1';
}
$wfacp_ps_active_radio_checkbox = '';
if ( false === $forceAllSetting ) {
	$wfacp_ps_active_radio_checkbox = 'wfacp_ps_active_radio_checkbox';
}

$hide_qty_switcher_cls = '';
if ( $hide_quantity_switcher == true ) {
	$hide_qty_switcher_cls = 'wfacp_hide_qty_switcher';
}

if ( 'hidden' == $type && true === $enable_delete_item && !is_null(WFACP_Common::get_cart_item_from_removed_items( $item_key ) )) {
	return;
}


$wfacp_you_save_text_html = 'wfacp_you_save_text_blank';
if ( $you_save_text_html != '' ) {
	$wfacp_you_save_text_html = '';
}
$innerClass       = [];
$innerClass       = [ $is_variation_error, $best_val_class, $best_value_position, $product_is_hide_cls, $product_selected_class ];
$innerClassString = implode( ' ', $innerClass );


add_filter( 'wp_get_attachment_image_attributes', 'WFACP_Common::remove_src_set' );
?>
    <fieldset class="woocommerce-cart-form__cart-item cart_item wfacp_product_row <?php echo trim( $innerClassString ); ?>" data-item-key="<?php echo $item_key; ?>" cart_key="<?php echo $cart_item_key; ?>" data-id="<?php echo $pro->get_id(); ?>">
		<?php
		if ( '' != $best_value && ( 'top_left_corner' == $best_value_position || 'top_right_corner' == $best_value_position ) ) {
			printf( "<legend class='wfacp_best_value wfacp_%s'>%s</legend>", $best_value_position, $best_value );
		}

		?>
        <div class="wfacp_row_wrap <?php echo $ps_cls . ' ' . $enable_hide_img . " " . $wfacp_you_save_text_html; ?>">
            <div class="wfacp_ps_title_wrap">
                <div class="wfacp_product_switcher_col wfacp_product_switcher_col_1 ">
                    <input id='wfacp_product_<?php echo $item_key; ?>' type="<?php echo $type; ?>" name="wfacp_product_choosen" class="<?php echo $input_class; ?> wfacp_switcher_checkbox input-checkbox" id="wfacp_product_choosen_<?php echo $item_key; ?>" data-item-key="<?php echo $item_key; ?>" <?php echo $product_data['is_checked']; ?> cart_key="<?php echo $cart_item_key; ?>">
					<?php
					if ( 'hidden' == $type && true === $enable_delete_item && true === wc_string_to_bool( $product_data['enable_delete'] ) && isset( WC()->cart->cart_contents[ $cart_item_key ] ) ) {
						$item_class = 'wfacp_remove_item_from_cart';
						$item_icon  = 'x';
						?>
                        <div class="wfacp_product_switcher_remove_product wfacp_delete_item">
                            <a href="javascript:void(0)" class="<?php echo $item_class; ?>" data-cart_key="<?php echo $cart_item_key; ?>" data-item_key="<?php echo $item_key; ?>"><?php echo $item_icon; ?></a>
                        </div>
						<?php
					}

					$merge_tag_quantity = WFACP_Common::product_switcher_merge_tags( "{{quantity}}", $price_data, $pro, $product_data, $cart_item, $cart_item_key );
					if ( false == $product_data['hide_product_image'] ) {
						$qtyHtml   = sprintf( '<div class="wfacp-qty-ball"><span class="wfacp-qty-count wfacp_product_switcher_quantity"><span class="wfacp-pro-count">%s</span></span></div>', $merge_tag_quantity );
						$thumbnail = $pro->get_image( [ 100, 100 ], [ 'srcset' => false ] );
						echo sprintf( '<div class="product-image"><div class="wfacp-pro-thumb">%s</div>%s</div>', $thumbnail, $qtyHtml );
					}
					?>
                </div>
                <div class="wfacp_product_switcher_col wfacp_product_switcher_col_2">
					<?php
					echo "<div class='wfacp_product_switcher_description'>";
					$variation_class = count( $variation_attributes ) > 0 ? 'wfacp_variation_product_title' : '';
					$best_value_html = '';
					if ( '' != $best_value ) {
						$best_value_html = sprintf( "<span class='wfacp_best_value wfacp_best_value_below'>%s</span>", $best_value );
					}
					if ( '' != $best_value && ( 'above' == $best_value_position ) ) {
						echo "<div class='wfacp_best_value_container'>" . $best_value_html . "</div>";
					}
					$best_value_default = ( '' != $best_value && '' == $best_value_position ) ? $best_value_html : '';
					if ( true == $product_data['hide_product_image'] ) {
						$product_title .= '<span class="wfacp_product_row_quantity wfacp_product_switcher_quantity"> x' . $merge_tag_quantity . "</span>";
					}
					echo sprintf( '<div class="product-name product_name"><div class="wfacp_product_sec"> <span class="wfacp_product_choosen_label_wrap %s"><span class="wfacp_product_choosen_label" for="%s">%s</span></span> %s</div></div>', $variation_class, "wfacp_product_{$item_key}", $product_title . " " . $best_value_default, $quick_preview . $choose_label );
					if ( '' != $best_value && 'below' == $best_value_position ) {
						echo "<div class='wfacp_best_value_container'>" . $best_value_html . "</div>";
					}


					echo '<div class="wfacp_ps_div_row">';
					if ( $you_save_text_html != '' ) {
						echo $you_save_text_html;
					}
					echo $subscription_product_string;
					echo '</div>';
					echo '</div>';
					if ( '' != $is_variation_error ) {
						?>
                        <div class="wfacp_invalid_variarion" style="display:none"><?php _e( 'Select', 'woofunnels-aero-checkout' ); ?>
                            <span class="wfacp_attributes_keys"><?php echo implode( '/', $attributes_keys ); ?></span>
                        </div>
						<?php
					}
					$cls_sold_individually = '';
					if ( isset( $is_sold_individually ) && $is_sold_individually == 1 ) {
						$cls_sold_individually = 'wfacp_sold_indi';
					}
					?>
                </div>
            </div>
			<?php

			?>
            <div class="wfacp_product_sec_start ">

				<?php
				$hide_qty_switcher_cls = '';
				if ( true == $hide_quantity_switcher ) {
					$hide_qty_switcher_cls = 'wfacp_hide_qty_switcher1';
				}
				?>
                <div class="wfacp_product_switcher_col wfacp_product_switcher_col_3 <?php echo $cls_sold_individually . " " . $hide_qty_switcher_cls; ?>">
                    <div class="wfacp_product_quantity_container">
						<?php
						if ( ! $pro->is_sold_individually() ) {
							$rqty     = 1;
							$qty_step = 1;
							if ( '' !== $cart_item_key ) {
								$qty_step = 0;
								$rqty     = $product_data['quantity'];
							}
							?>
                            <div class="wfacp_quantity_selector" style="<?php echo ( true != $hide_quantity_switcher ) ? 'display:flex' : 'display:none;pointer-events:none;'; ?>">
                                <input type="number" step="1" min="<?php echo $qty_step; ?>" value="<?php echo $rqty; ?>" data-value="<?php echo $rqty; ?>" name="wfacp_product_switcher_quantity_<?php echo $item_key; ?>" class="wfacp_product_switcher_quantity" onfocusout="this.value = (Math.abs(this.value)<0?0:Math.abs(this.value))">
                            </div>
							<?php
						} elseif ( $is_sold_individually ) {
							?>
                            <span class="wfacp_sold_individually">1</span>
							<?php
						}
						?>
                    </div>
                    <div class="wfacp_product_price_container product-price">
                        <div class="wfacp_product_price_sec">
							<?php
							$price_html = '';
							if ( in_array( $pro->get_type(), WFACP_Common::get_subscription_product_type() ) ) {
								if ( '' !== $cart_item_key ) {
									$price_html = wc_price( $price_data['price'] );
								} else {
									$price_html = wc_price( WFACP_Common::get_subscription_price( $pro, $price_data ) );
								}
							} else {
								if ( $price_data['regular_org'] == 0 ) {
									echo $pro->get_price_html();
								} else {
									if ( $price_data['price'] > 0 && $price_data['regular_org'] > 0 && ( absint( $price_data['price'] ) !== absint( $price_data['regular_org'] ) ) ) {
										$price_html = wc_format_sale_price( $price_data['regular_org'], $price_data['price'] );
									} else {
										$price_html = wc_price( $price_data['price'] );
									}
								}
							}
							echo apply_filters( 'wfacp_product_switcher_price_text', $price_html, $pro, $price_data, $product_data );
							?>


                        </div>


                    </div>
					<?php
					if ( 'hidden' == $type && true === $enable_delete_item && true === wc_string_to_bool( $product_data['enable_delete'] ) && isset( $cart_item_key ) && '' != $cart_item_key ) {
						$item_class = 'wfacp_remove_item_from_cart';
						$item_icon  = 'x';
						?>
                        <div class="wfacp_crossicon_for_mb">
                            <div class="wfacp_product_switcher_remove_product wfacp_delete_item">
                                <a href="javascript:void(0)" class="<?php echo $item_class; ?>" data-cart_key="<?php echo $cart_item_key; ?>" data-item_key="<?php echo $item_key; ?>"><?php echo $item_icon; ?></a>
                            </div>
                        </div>
						<?php
					}
					?>


                </div>
            </div>

        </div>


    </fieldset>
<?php
remove_filter( 'wp_get_attachment_image_attributes', 'WFACP_Common::remove_src_set' );
?>
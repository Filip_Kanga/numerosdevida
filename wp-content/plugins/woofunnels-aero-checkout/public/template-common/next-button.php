<?php
defined( 'ABSPATH' ) || exit;
$instance    = WFACP_Core()->customizer->get_template_instance();
$next_action = 'two_step';
$data_text   = 'step-2';
if ( $current_action === 'two_step' ) {
	$next_action = 'third_step';
	$data_text   = 'step-3';
}
$change_next_btn = apply_filters( 'wfacp_change_next_btn_' . $current_action, 'Next', $current_action );
?>
<div class="wfacp-two-step-erap wfacp-next-btn-wrap">
    <button type="button" class="button button-primary wfacp_next_page_button" data-next-step="<?php echo $next_action; ?>" data-current-step='<?php echo $current_action; ?>' value="<?php _e( 'Next Step', 'woofunnels-aero-checkout' ); ?>" data-text="<?php echo $data_text ?>">
		<?php _e( $change_next_btn, 'woofunnels-aero-checkout' ); ?>
    </button>
</div>

<?php
/**
 * ========================
 * Quick View Template
 * ========================
 * */
//Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	return;
}
global $product;


add_action( 'wfacp_qv_summary', 'woocommerce_template_single_title', 5 );
//add_action( 'wfacp_qv_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'wfacp_qv_summary', 'woocommerce_template_single_price', 10 );
add_action( 'wfacp_qv_summary', [ WFACP_Core()->public, 'woocommerce_template_single_excerpt' ], 20 );
add_action( 'wfacp_qv_summary', [ WFACP_Core()->public, 'woocommerce_template_single_add_to_cart' ], 25 );
add_action( 'wfacp_qv_summary', function () {
	global $product, $wfacp_product;
	if ( ! is_null( $wfacp_product ) ) {
		$shortDescription = $wfacp_product->get_description();
		if ( '' == $shortDescription ) {
			$shortDescription = $product->get_description();
		}
		echo $shortDescription;
	}
}, 18 );


remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );
// hide update qty input field for single variation and simple product type
//add_action( 'wfacp_woocommerce_simple_add_to_cart', [ WFACP_Core()->public, 'woocommerce_simple_add_to_cart' ] );
add_action( 'woocommerce_single_variation', [ WFACP_Core()->public, 'woocommerce_single_variation_add_to_cart_button' ], 20 );

add_action( 'wfac_qv_images', function () {
	include_once WFACP_TEMPLATE_COMMON . '/quick-view/images/product-image.php';
}, 20 );


global $wfacp_product, $product;

if ( is_null( $wfacp_product ) ) {
	add_action( 'wfacp_woocommerce_variable_add_to_cart', [ WFACP_Core()->public, 'woocommerce_variable_add_to_cart' ] );
	add_action( 'wfacp_woocommerce_variable-subscription_add_to_cart', [ WFACP_Core()->public, 'woocommerce_variable_subscription_add_to_cart' ] );
} else {
	//  add_action( 'wfacp_woocommerce_variable_add_to_cart', [ WFACP_Core()->public, 'woocommerce_simple_add_to_cart' ] );
	//  add_action( 'wfacp_woocommerce_variable-subscription_add_to_cart', [ WFACP_Core()->public, 'woocommerce_subscription_add_to_cart' ] );
}
add_filter( 'woocommerce_single_product_flexslider_enabled', function () {
	return true;
} );

$productType = '';
if ( $product instanceof WC_Product ) {
	$productType = "wfacp_type_" . $product->get_type();
}


?>
<style>
    #wfacp_qr_model_wrap .woocommerce-product-gallery__image a img {
        display: block;
        width: 100%;
        height: auto;
        box-shadow: none;
    }

    .single_variation_wrap .woocommerce-variation-price {
        display: none;
    }

    .wfacp_option_btn {
        display: none;
    }

    @media (max-width: 767px) {
        .wfacp_option_btn {
            display: block;
        }

        .wfacp_option_btn, #wfacp_qr_model_wrap .wfacp_qr_wrap .button {
            background: #24ae4e;
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            color: #fff;
            padding: 14px 10px;
            font-size: 17px;
            cursor: pointer;
        }

        #wfacp_qr_model_wrap .wfacp_qr_wrap .button {
            position: fixed;
            width: 100%;
        }

        .wfacp_qv-main {
            padding: 10px 10px 35px;
        }
    }
</style>
<div id="wfacp_qr_model_wrap" class=" wfacp_qv-inner-modal <?php echo $productType; ?>" data-item-key="<?php echo $item_key; ?>" data-cart-key="<?php echo $cart_key; ?>">
    <div class="wfacp_qv-container woocommerce single-product">
        <div class="wfacp_qv-top-panel">
            <div class="wfacp_qv-close wfacp_qv xooqv-cross"></div>
            <div class="wfacp_qv-preloader wfacp_qv-mpl">
                <div class="wfacp_qv-speeding-wheel"></div>
            </div>
        </div>
        <div class="wfacp_qv-main">
            <div>
                <div class="wfacp_qr_wrap product">
                    <div class="wfacp_qv-images">
						<?php do_action( 'wfac_qv_images' ); ?>
                    </div>
                    <div class="wfacp_qv-summary">
						<?php
						/**
						 * @todo
						 * Using our custom hook display only few content
						 * some themes like flatsome changes the normal behaviour of components
						 *
						 */
						?>
						<?php do_action( 'wfacp_qv_summary' ); ?>
                    </div>

                    <div class="wfacp_clear"></div>
                </div>
            </div>
        </div>
        <div class="wfacp_option_btn"><?php _e('Choose an Option','woofunnels-aero-checkout');?></div>
    </div>
</div>

<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$instance = WFACP_Core()->customizer->get_template_instance();
if ( is_null( $instance ) ) {
	return '';
}


$data = $instance->get_checkout_fields();
if ( ! isset( $data['advanced']['shipping_calculator'] ) ) {
	return;
}
$field = $data['advanced']['shipping_calculator'];

$placeholder = WFACP_Common::default_shipping_placeholder_text();
if ( isset( $field['default'] ) && '' !== $field['default'] ) {
	$placeholder = $field['default'];
}


$class = 'wfacp_single_methods';
if ( 1 < count( $available_methods ) ) {
	$class = 'wfacp_multi_methods';
}
$colspan              = apply_filters( 'wfacp_shipping_col_span', '' );
$display_package_name = apply_filters( 'wfacp_show_shipping_package_name', false, $available_methods );

?>
<tr class="shipping <?php echo $class; ?> <?php echo $display_package_name ? 'wfacp_package_name_display' : ''; ?>">


    <td class=" <?php echo $display_package_name ? 'wfacp_shipping_package_name' : ''; ?>" data-title="<?php echo esc_attr( $package_name ); ?>" <?php echo $colspan; ?>>
		<?php
		if ( $display_package_name ) {
			?>
            <p class=""><?php echo wp_kses_post( $package_name ); ?></p>
			<?php
		}
		if ( 1 < count( $available_methods ) ) :
			?>
            <ul id="shipping_method">
				<?php
				foreach ( $available_methods as $method ) :
					$shipping_cost = '';
					$shippingLable = "";
					$shipping_cost = floatval( $method->get_cost() );
					if ( $shipping_cost <= 0 ) {
						$shippingLable = sprintf( " : %s", __( 'Free', 'woocommerce' ) );
					}
					?>
                    <li>
						<?php
						printf( '<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s />
								<label for="shipping_method_%1$d_%2$s">%5$s</label>', $index, sanitize_title( $method->id ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ), wc_cart_totals_shipping_method_label( $method ) . $shippingLable );
						do_action( 'woocommerce_after_shipping_rate', $method, $index );
						?>
                    </li>
				<?php endforeach; ?>
            </ul>
		<?php elseif ( 1 === count( $available_methods ) ) :
			?>
            <ul id="shipping_method">
                <li>
					<?php
					$method        = current( $available_methods );
					$shipping_cost = '';
					$shippingLable = "";

					$shipping_cost = floatval( $method->get_cost() );
					if ( $shipping_cost <= 0 ) {
						$shippingLable = sprintf( " : %s", __( 'Free', 'woocommerce' ) );
					}


					printf( '%3$s <input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d" value="%2$s" class="shipping_method" />', $index, esc_attr( $method->id ), wc_cart_totals_shipping_method_label( $method ) . $shippingLable );
					do_action( 'woocommerce_after_shipping_rate', $method, $index );
					?>
                </li>
            </ul>
		<?php elseif ( WC()->customer->has_calculated_shipping() ) : ?>
            <ul id="shipping_method">
                <li>
					<?php
					if ( is_cart() ) {
						echo apply_filters( 'woocommerce_cart_no_shipping_available_html', wpautop( __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
					} else {
						echo apply_filters( 'woocommerce_no_shipping_available_html', wpautop( __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
					}
					?>
                </li>
            </ul>
		<?php elseif ( ! is_cart() ) : ?>
            <ul id="shipping_method" class="wfacp_no_add_here">
                <li>
                    <p><?php echo apply_filters( 'wfacp_default_shipping_message', $placeholder ); ?></p>
                </li>
            </ul>
		<?php endif; ?>

		<?php if ( $show_package_details ) : ?>
            <ul id="shipping_method">

                <li>
					<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?></li>
            </ul>
		<?php endif; ?>
    </td>
</tr>

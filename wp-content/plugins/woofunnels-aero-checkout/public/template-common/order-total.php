<?php
defined( 'ABSPATH' ) || exit;
$args = WC()->session->get( 'wfacp_order_total_field', [] );

$colspan_attr = '';
if ( apply_filters( 'wfacp_cart_show_product_thumbnail', false ) ) {
	$colspan_attr1    = ' colspan="2"';
	$colspan_attr     = apply_filters( 'wfacp_order_summary_cols_span', $colspan_attr1 );
	$cellpadding_attr = ' cellpadding="20"';
}


$show_breakups =  wc_string_to_bool( $args['default'] );
?>
<div class="wfacp_order_total" style="clear:both">
	<?php

	if ( false == $show_breakups ) {
		?>
        <div class="wfacp_order_sub_total_container">
            <div class="wfacp_order_total_label"><?php echo __( 'Subtotal', 'woocommerce' ); ?></div>
            <div class="wfacp_order_total_value"><?php echo wc_price( WC()->cart->get_subtotal() ); ?></div>
        </div>
        <div class="wfacp_clear"></div>
		<?php
		foreach ( WC()->cart->get_coupons() as $code => $coupon ) {
			?>
            <div class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                <div class="wfacp_order_total_label"><?php wc_cart_totals_coupon_label( $coupon ); ?></div>
                <div class="wfacp_order_total_value"><?php wc_cart_totals_coupon_html( $coupon ); ?></div>
            </div>
			<?php
		}
		?>
        <div class="wfacp_clear"></div>
		<?php

		foreach ( WC()->cart->get_fees() as $fee ) { ?>
            <div class="fee">
                <div class="wfacp_order_total_label" <?php echo $colspan_attr; ?>><?php echo esc_html( $fee->name ); ?></div>
                <div class="wfacp_order_total_value"><?php wc_cart_totals_fee_html( $fee ); ?></div>
            </div>
			<?php
		}
		?>
        <div class="wfacp_clear"></div>
		<?php
		if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) {
			do_action( 'woocommerce_review_order_before_shipping' );
			echo "<table class='wfacp_order_total_shipping_table'>";
			WFACP_Common::wc_cart_totals_shipping_html( $colspan_attr );
			echo "</table>";
			do_action( 'woocommerce_review_order_after_shipping' );
		}
		?>
        <div class="wfacp_clear"></div>
		<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
            <div class='wfacp_order_total_tax_section'>
				<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
					<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
                        <div class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
                            <div class="wfacp_order_total_label"<?php echo $colspan_attr; ?>><?php echo esc_html( $tax->label ); ?></div>
                            <div class="wfacp_order_total_value"><?php echo wp_kses_post( $tax->formatted_amount ); ?></div>
                        </div>
					<?php endforeach; ?>
				<?php else : ?>
                    <div class="tax-total">
                        <div class="wfacp_order_total_label"<?php echo $colspan_attr; ?>><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></div>
                        <div class="wfacp_order_total_value"><?php wc_cart_totals_taxes_total_html(); ?></div>
                    </div>
				<?php endif; ?>
            </div>
		<?php endif; ?>
        <div class="wfacp_clear"></div>
	<?php } ?>
    <div class="wfacp_order_total_container clearfix">
        <div class="wfacp_order_total_label"><?php echo isset( $args['label'] ) ? $args['label'] : __( 'Order Total', 'woocommerce' ); ?></div>
        <div class="wfacp_order_total_value"><?php echo wc_price( WC()->cart->total ); ?></div>
    </div>
</div>

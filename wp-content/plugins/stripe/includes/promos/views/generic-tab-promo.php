<h2><?php _e( 'Want to customize your payment forms even more?', 'stripe' ); ?></h2>
<p>
	<?php _e( 'By upgrading to WP Simple Pay Pro, you get access to powerful features such as:', 'stripe' ); ?>
</p>

<!-- Repeat this bulleted list in sidebar.php & generic-tab-promo.php -->
<ul>
	<li><div class="dashicons dashicons-yes"></div> <?php _e( 'Unlimited Custom Fields', 'stripe' ); ?></li>
	<li><div class="dashicons dashicons-yes"></div> <?php _e( 'User-Entered Amounts', 'stripe' ); ?></li>
	<li><div class="dashicons dashicons-yes"></div> <?php _e( 'Coupon Codes', 'stripe' ); ?></li>
	<li><div class="dashicons dashicons-yes"></div> <?php _e( 'Embedded & Overlay Custom Forms', 'stripe' ); ?></li>
	<li><div class="dashicons dashicons-yes"></div> <?php _e( 'Apple Pay & Google Pay', 'stripe' ); ?></li>
	<li><div class="dashicons dashicons-yes"></div> <?php _e( 'Stripe Subscription Support (Plus license required)', 'stripe' ); ?></li>
</ul>

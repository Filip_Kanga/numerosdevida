<div class="simpay-promo-under-box">

	<h2><?php _e( 'Need your customers to sign up for recurring payments?', 'stripe' ); ?></h2>

	<p>
		<?php _e( 'By upgrading to a WP Simple Pay Pro Plus or higher license, you can connect Stripe subscriptions to your payment forms. You can also create installment plans, setup fees, and free trial periods.', 'stripe' ); ?>
	</p>

	<p>
		<a href="<?php echo simpay_pro_upgrade_url( 'under-box-promo' ); ?>"
		   class="simpay-upgrade-btn simpay-upgrade-btn-large" target="_blank" rel="noopener noreferrer">
			<?php _e( 'Click here to Upgrade', 'stripe' ); ?>
		</a>
	</p>

</div>

<?php

function fkwp_vwo_rev()
{
    wp_enqueue_script( 'vwo_rev', plugin_dir_url(__FILE__) . '/assets/vwo_revenue.js', array(), '1.0.0', false );
}

add_action( 'wp_enqueue_scripts', 'fkwp_vwo_rev' );
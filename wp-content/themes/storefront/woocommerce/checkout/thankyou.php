<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<style>
	body { background: url('<?php echo get_site_url(); ?>/wp-content/uploads/2019/03/rd_wl_landing_bg2-min.jpg') center top no-repeat fixed; background-size: cover; }
</style>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '394139377851633');
  fbq('track', 'PageView');
fbq('track', 'Purchase', {
    value: 33,
    currency: 'USD',
  });
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=394139377851633&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<p>
<h3>
    Pedido recibido ! 
Gracias. Tu pedido ha sido recibido. 
¡Por favor revise su correo electrónico para su informe de lujo y bonificaciones!    
</h3>
</p>

<form name="ctl00" method="post" id="ctl00" enablevalidation="true">

    <div id="mainWrapper" class="bground_color">
		    <div id="wrapper">

                <div style="text-align:center; padding-top:50px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:52px;"><span style="font-size:56px; color:#08AAC3;">PARABÉNS </span><br />



                <div style="font-family:Arial, Helvetica, sans-serif; font-size:17px; color:#111111; font-weight:bold; text-align:center; padding-top:20px;">
	                SU PEDIDO
                    <div style="width: 100%; text-align: center;"> <div>

	<table cellspacing="0" cellpadding="5" rules="all" border="1" id="gvOrderDetails" style="border-color:Black;border-width:1px;border-style:Solid;width:90%;border-collapse:collapse;margin: 0 auto; background: #fff;">

		<tr style="background-color:#08AAC3;">
			<th scope="col" style="border-width:1px;border-style:solid;">Nombre del producto</th><th scope="col" style="border-width:1px;border-style:solid;">Cantidad </th><th scope="col" style="border-width:1px;border-style:solid;">Total</th>
     	</tr>
		<?php 
			
				if($order)
				{
					$get_items = $order->get_items();	
					
					if ( count($get_items) > 0 ) {
						$order->calculate_totals();
					}
					
					// Iterate though each order item
					foreach ($get_items as $item_id => $item) 
					{
						echo "<tr>";
						// Get the WC_Order_Item_Product object properties in an array
					    $item_data = $item->get_data();
					
					    if ($item['quantity'] > 0) 
					    {
					        // get the WC_Product object
					        $product = wc_get_product($item['product_id']);
					        
					        $product_name = $product->get_name();
					        
					        $product_img = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'medium' )[0];
					        $quantity = '';
					        
					        if($product->get_id() == 38) {
								$quantity = "<span class='quantity_circle center'>6</span>";
							} else if($product->get_id() == 37) {
								$quantity = "<span class='quantity_circle center'>3</span>";
							} else if($product->get_id() == 36) {
								$quantity = "<span class='quantity_circle center'>1</span>";
							}
							
					        echo "<td><img src='{$product_img}' class='product_img'><p>{$product_name}</p></td>";
					        echo "<td>1</td>";
					        echo "<td>{$item->get_total()}</td>";
							
					        
					    }
					    
					    echo "</tr>";
					}
				}
			
			?>

	</table>

</div>

                    </div>
                    
                    <?php
                    
						$order_data = $order->get_data();
						
						$order_shipping_first_name = $order_data['shipping']['first_name'];
						$order_shipping_last_name = $order_data['shipping']['last_name'];
						$order_shipping_address_1 = $order_data['shipping']['address_1'];
						$order_shipping_city = $order_data['shipping']['city'];
						$order_shipping_state = $order_data['shipping']['state'];
						$order_shipping_postcode = $order_data['shipping']['postcode'];
						$order_shipping_country = $order_data['shipping']['country'];
						
					?>

                    SERÁ ENVIADO PARA:

	                <div class="order-details" style="max-width: 90%; margin: 0 auto;">
	                	
	                <div class='full_name_address'>
                    	<p>
                    		<?php
                    			
                    			echo "<h3>" . $order_shipping_first_name . " " . $order_shipping_last_name . "</h3>";
                    		?>
                    	</p>
                    	<p>
                    		<?php 
                    			echo "<h3>" . $order_shipping_address_1 . ", " . $order_shipping_city . ", " . $order_shipping_state . ", " . $order_shipping_postcode . ", " .  $order_shipping_country . "</h3>";
                    		?>
                    	</p>
                    </div>	
	                	
	                	
	                	
<!--	                	<div class="table-responsive">-->
<!--  <table class="table table-bordered">-->
<!--    <thead>-->
<!--      <tr style="background: #08aac3; color: #000000;">-->
<!--      	<th scope="col">#</th>-->
<!--        <th scope="col">Nome</th>-->
<!--        <th scope="col">Sobrenome</th>-->
<!--        <th scope="col">Endereco</th>-->
<!--        <th scope="col">Citate</th>-->
<!--        <th scope="col">Estado</th>-->
<!--        <th scope="col">CEP</th>-->
<!--        <th scope="col">País</th>-->
<!--      </tr>-->
<!--    </thead>-->
<!--    <tbody style='background-color: white;'>-->
<!--      <tr>-->
<!--        <th scope="row">1</th>-->
<!--        <td><?php //echo   $order_shipping_first_name   ?></td>-->
<!--        <td><?php //echo   $order_shipping_last_name   ?></td>-->
<!--        <td><?php //echo   $order_shipping_address_1   ?></td>-->
<!--        <td><?php //echo   $order_shipping_city   ?></td>-->
<!--        <td><?php //echo   $order_shipping_state   ?></td>-->
<!--        <td><?php //echo   $order_shipping_postcode   ?></td>-->
<!--        <td><?php //echo   $order_shipping_country   ?></td>-->
<!--      </tr>-->
      
      
<!--    </tbody>-->
<!--  </table>-->
<!--</div>-->
	                	
	               
	                	
							<!-- <table class="table table-bordered table_pos dose-order-customer-table">-->
							<!--  <thead>-->
							<!--    <tr>-->
							    	
							      
							<!--      <th class="th_color" scope="col">Nome</th>-->
							<!--      <th class="th_color" scope="col">Sobrenome </th>-->
							<!--      <th class="th_color" scope="col">Endereco</th>-->
							<!--      <th class="th_color" scope="col">Citate</th>-->
							<!--      <th class="th_color" scope="col">Estado</th>-->
							<!--      <th class="th_color" scope="col">CEP</th>-->
							<!--      <th class="th_color" scope="col">País</th>-->
							<!--    </tr>-->
							<!--  </thead>-->
							<!--  <tbody>-->
							<!--    <tr>-->
							<!--      <td><?php //echo   $order_shipping_first_name   ?></td>-->
							<!--      <td><?php //echo   $order_shipping_last_name   ?></td>-->
							<!--      <td><?php //echo   $order_shipping_address_1   ?></td>-->
							<!--      <td><?php //echo   $order_shipping_city   ?></td>-->
							<!--      <td><?php //echo   $order_shipping_state   ?></td>-->
							<!--      <td><?php //echo   $order_shipping_postcode   ?></td>-->
							<!--      <td><?php //echo   $order_shipping_country   ?></td>-->
							<!--    </tr>-->
							<!--  </tbody>-->
							<!--</table>-->
                            
                            

	                </div>

                </div>

            </div>
            
		    </div><!-- END wrapper -->

        <div class="home_ty" style='text-align: center'>
	      	<b><a href='<?php echo get_site_url(); ?>'>HOME</a></b>
        </div>
 
    </div><!-- END mainWrapper -->
</form>


</div>

<div id="grid-row-3" class="pure-g" style="width: 100%; max-width:1460px !important;">
<div id="row-3-col-1" class="pure-u-1-1 pure-u-sm-1-1">
<div class="greenfooter">
	<nav>
		<a href="<?php echo get_site_url(); ?>/terminos-y-condicioness/" target="_blank">Términos y Condicioness</a> | 
		<a href="<?php echo get_site_url(); ?>/politica-de-privacidad/" target="_blank">Política de privacidad</a> | 
		<a href="<?php echo get_site_url(); ?>/contactanos/" target="_blank">Contactanos</a>
		<div class="cleared"></div>
	</nav>
	<section>
		<p>As afirmações a respeito destes produtos não foram avaliadas pela Agência Nacional de Vigilância Sanitária. Estes produtos não pretendem diagnosticar, tratar, curar ou prevenir nenhuma doença. As informações contidas neste site ou nos e-mails enviados foram elaboradas apenas para propósitos educacionais. Não existe intenção de substituir conselhos ou tratamentos médicos adequados. Estas informações não devem ser utilizadas para diagnosticar ou tratar qualquer problema de saúde ou doença sem uma consulta médica. Você também pode visitar nossa Home Page, ler artigos, aprender mais sobre a Dose Perfeita ou fazer seu pedido.</p>
	</section>
</div>

</div><!-- end row-3-col-1 -->
</div><!-- end grid-row-3 -->

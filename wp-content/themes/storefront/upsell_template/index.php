<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Numeros de vida</title>
    <meta name="description" content="{pagemetadescription}" />
    <link rel="icon" type="image/x-icon" href="//images.riskhedge.com/media/favicons/favicon.ico" />
    <link rel="icon" type="image/png" sizes="192x192" href="//images.riskhedge.com/media/favicons/riskhedge-favicon-192x192.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="//images.riskhedge.com/media/favicons/riskhedge-favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="//images.riskhedge.com/media/favicons/riskhedge-favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="//images.riskhedge.com/media/favicons/riskhedge-favicon-16x16.png" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,700&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet"> 
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>

    <base href="<?php echo get_template_directory_uri() . '/upsell_template/'; ?>">

    <script>
      window.addEventListener('DOMContentLoaded', (event) => {   


        var name = localStorage.getItem("name");
        var middle_name = localStorage.getItem("fullname");
        var date = localStorage.getItem("dob");
        var gender = localStorage.getItem("gender");
        var marital = localStorage.getItem("marital");
        var email = localStorage.getItem("email");

        if ( Object.prototype.toString.call(date) !== "[object Date]" ) {
            date = new Date(date);
        }


        date = new Date(date);
        // console.table({day: date.getDate(), month: date.getMonth()+1, year: date.getFullYear()});

        function sum_digits(digits) {

          let output = [];

          let dgts = digits.toString();          

          for (let i = 0, len = dgts.length; i < len; i += 1) {
              output.push(dgts[i]);
          }

          let sum = 0;          

          //sum the digits
          for(let j = 0; j < output.length; j++) {
              sum += parseInt(output[j]);
          }

          return sum;

        }  

        var numero = parseInt(sum_digits(date.getDate()) + sum_digits(date.getMonth()+1) + sum_digits(date.getFullYear()));

        if(numero > 10 && numero != 11 && numero != 22 && numero != 33) {
            numero = parseInt(sum_digits(numero));
        }



        //change paragraph based on number        
        // console.log(numero);
        switch(numero) {
          case 1: { document.getElementById('nd_num_base').innerHTML = `Eres capaz de hacer las cosas a tu propio ritmo, eres dueño de una gran auto-motivación y posees un espíritu independiente. También gozas de una gran creatividad que viene acompañada de una férrea determinación  en tu vida. Eres del tipo de persona que genera cambios, que empieza las cosas, es por esto que una constante es tu deseo de ser el número 1.
De acuerdo a tu número de vida “<b class='nd_num'>8</b>”, si éste se encuentra fuera de balance, tiendes a ser egocéntrico y fijarte sólo en ti. Te cuesta mucho seguir a otros y la gente te puede percibir como mandón y dominante. 
`; break; }
          case 2: { document.getElementById('nd_num_base').innerHTML = `Tienes en ti la energía dual, de modo que puedes ver todas las perspectivas de cualquier asunto, esto hace que de forma natural seas un mediador, alguien que resuelve conflictos.  Eres de naturaleza honesta y piensas lo mejor de los demás, gracias a tu encanto en lugar de parecer impositivo te ven como alguien con gran persuasión.
De acuerdo a tu número de vida  “<b class='nd_num'>8</b>” , también te cuesta mucho trabajo el expresar tus necesidades y hacer saber cuando algo no te gusta, lo que suele resultar en una gran explosión. Puede que seas demasiado tímido y sensible y que antepongas las necesidades de otros a las tuyas propias. 
`; break; }
          case 3: { document.getElementById('nd_num_base').innerHTML = `Tu vibración es fuerte, la creatividad y la expresión fluyen en tu ser, eres un verdadero jugador de equipo y las situaciones sociales se te dan más que bien. Eres una persona cálida y que está llena de optimismo. Podrías tener un buen desempeño como escritor o poeta.
De acuerdo a tu número de vida  “<b class='nd_num'>8</b>” ABC,  puedo ver que ya que tienes tanta energía esta tiende a dispersarse, lo cual te deja sin un propósito claro en tu vida. Si es que te llegan a lastimar, tú te escondes y huyes, volviéndote irritable, por tu naturaleza es común que atraigas a parejas que son muy demandantes. 
`; break; }
          case 4: { document.getElementById('nd_num_base').innerHTML = `Tú tienes un profundo entendimiento de cómo es que funcionan las cosas y puedes sin mayor problema ni mayor esfuerzo darte apoyo tanto a ti mismo como a quienes te rodean.
De acuerdo  a tu número de vida   “<b class='nd_num'>8</b>”,  puedo ver que sueles tener problemas con las autoridades. Sabes en el fondo que tú naciste para estar al mando, pero en lugar de eso tienes a otros mandándote. Trabajas demasiado duro y no se te da el respeto y la admiración que mereces. La vida se siente restrictiva y no sabes cómo liberarte. 
`; break; }
          case 5: { document.getElementById('nd_num_base').innerHTML = `Dotado con un alma aventurera, siempre buscas que la libertad sea lo más importante y constante en tu vida. Tienes una necesidad por el cambio, lo novedoso, las cosas nuevas, nada de reglas asfixiantes o trabajos aburridos y repetitivos.  Tu amor por la libertad no solo te incluye a ti, deseas esta misma libertad para los demás.
De acuerdo a tu número de vida  “<b class='nd_num'>8</b>”,  puede ser que esta imperiosa necesidad por la aventura y la libertad te deje inquieto y con poco o nulo sentido de dirección en tu vida. En las relaciones amorosas puede que te sientas sofocado, y ya que vas de una cosa a otra, muchas veces no terminas nada. 
`; break; }
          case 6: { document.getElementById('nd_num_base').innerHTML = `Eres bastante sensitivo y empático con el don de hacer felices a todos aquellos que te rodean. Eres responsable, te manejas con certeza  y estás al mando en todos los sentidos.
De acuerdo a tu número de vida “<b class='nd_num'>8</b>” puedo ver que te comparas con otros constantemente  y sientes que no eres lo suficientemente bueno. Abarcas más de lo que puedes porque piensas que los demás lo dejarán a medias. “Si quieres algo bien hecho hazlo tu mismo” es tu lema, pero puede drenarte. Eres un perfeccionista y por eso renuncias antes de siquiera comenzar. 
`; break; }
          case 7: { document.getElementById('nd_num_base').innerHTML = `No es fácil que tea abras con la gente, toma bastante tiempo, pero si dejas que alguien sea tu amigo, lo sea para siempre.  Tienes un talento nato para la investigación y el análisis y esto aplica con la gente también. Prefieres evitar las multitudes y el escándalo.
De acuerdo a tu número de vida  “<b class='nd_num'>8</b>”  por el hecho de pasar tanto tiempo a solas, te puedes volver una persona egoísta y desconsiderada. Te vas mucho por el lado del pesimismo y sueles ser inflexible ya que te acostumbras a sólo hacerte caso a ti mismo.
`; break; }
          case 8: { document.getElementById('nd_num_base').innerHTML = `Tienes pasión, poder, magnetismo y puedes ver el panorama más amplio que otros pasan de largo. Eres un líder natural y tu carisma es impresionante, esto puede llevarte muy lejos.
De acuerdo a tu número de vida “<b class='nd_num'>8</b>” puedo ver que a menudo desconfías de las personas con dinero, poder e influencia.  Tienes a valorar menos tu éxito por el temor de que los demás te juzguen como alguien arrogante y avaricioso. No conoces tus límites e intentas hacer todo por todos los demás. 
`; break; }
          case 9: { document.getElementById('nd_num_base').innerHTML = `Eres altamente digno de confianza, haces amigos fácilmente gracias a tu naturaleza amable y tu corazón abierto. Eres una persona que da y poses una gran empatía para con los demás. Eres muy amigable y tienes una presencia que impone, la gente suele confiar en ti.  Este es el número de un solo digito más alto que se puede tener.
De acuerdo a tu número de vida  “<b class='nd_num'>8</b>”  eres el tipo de persona que puede rápidamente perder la fe y es posible que en tu misión de ayudar a los demás, gastes mucho más de lo que ahorres y no tengas unas finanzas tan sólidas.  Puede ser que seas muy demandante.
`; break; }
          case 11: { document.getElementById('nd_num_base').innerHTML = `Eres una persona muy sabia y tienes una gran intuición y habilidades psíquicas. También cuentas con un sentido de conocimiento y puedes predecir cosas antes de que sucedan.
De acuerdo a tu número de vida “<b class='nd_num'>8</b>”, puedo ver que te cuesta dejar ir el pasado y esto complica mucho el que disfrutes tu presente y puedas crear un futuro más positivo y lleno de amor. Le temes al abandono, al rechazo, a la vergüenza y sientes que no encajas. 
`; break; }
          case 22: { document.getElementById('nd_num_base').innerHTML = `Se te ha dado el don para un éxito muy grande en la vida, tienes también una enorme capacidad de entendimiento en tu ser.  Eres tanto un soñador como una persona bien terrenal, puedes hacer que las ideas tomen forma y sean tangibles.
De acuerdo a tu número de vida  “<b class='nd_num'>8</b>”  te presionas demasiado y sueles cargar con culpas que no te corresponden. No sabes lidiar con los fracasos y puede que te abrume tu tremendo potencial y no hagas nada hasta que madures lo suficiente.
`; break; }
          case 22: { document.getElementById('nd_num_base').innerHTML = `Tu energía es única, el tener este número ya es en si muy raro. Eres una persona con una sola meta, mejorar a la humanidad en amor y elevar la vibración del mundo entero. Lo das todo cuando es la hora de hacerlo.
De acuerdo a tu número de vida  “<b class='nd_num'>8</b>” uno de los lados negativos que puedes llegar a tener es querer hacerlo todo por  tu cuenta. Puede ser que te compares con otros que percibas con más logros y te creas insuficiente. Tiendes al perfeccionismo y a no estar satisfecho. 
`; break; }
        }










        var dates = document.getElementsByClassName("nd_date");
        var names = document.getElementsByClassName("nd_name");
        var nums = document.getElementsByClassName("nd_num");

        // console.log(dates);

        [].slice.call( dates ).forEach(function ( el ) {
          el.innerHTML = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
        });

        [].slice.call( names ).forEach(function ( el ) {
          el.innerHTML = name;
        });

        [].slice.call( nums ).forEach(function ( el ) {
          el.innerHTML = numero;
        });
        
        
        
        /* == Scroll down */
        

      });
      
      function scroll_to_order() {
          var elmnt = document.getElementById("down_order");
          elmnt.scrollIntoView();
      }
      

    </script>

  </head>
  <body>


<style type="text/css">
       

      body {
        font-size: 18px;
        font-family: 'Quicksand', sans-serif;
        padding: 0;
        overflow-x: hidden;
        /*background-image: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('https://ggc-riskhedge-images.s3.amazonaws.com/media/backgroundi_aq.jpg');*/
        background-size: 100%;
        background-position: center center, center center;
        background-attachment: fixed;
        background-repeat: no-repeat;
        background-size: cover;
      }
      h1, h2, h3, h4, h5 {
        font-family: 'Bree Serif', serif;
      }

      h4 {
        line-height: 36px;
      }

      p {
        line-height: 1.3rem;
      }
      .mg_bot1 {
        margin-bottom: 1rem !important;
      }
      .mg_bot2 {
        margin-bottom: 2rem !important;
      }
      .mg_bot3 {
        margin-bottom: 3rem !important;
      }
      .bg_primary {
        background-color: #840C66;
      }
      .col_primary {
        color: #840C66;
      }

      .col_secondary {
        color: #260d60;
      }

      .white_col {
        color: #fff !important;
      }
      .f400 {
        font-weight: 400;
      }
      .f600 {
        font-weight: 600;
      }

      .f900 {
        font-weight: 900;
      }

      .font_115 {
        font-size: 115%;
      }
      .font_90 {
        font-size: 90%;
      }
      .center {
        display: block;
        margin: 0 auto;
      }
      .no_mg {
        margin: 0;
      }

      .btn {
        font-family: 'Quicksand', sans-serif;
      }

      .entry_heading {
        padding: 0 4%;
        margin: 2rem auto;
      }

      .price_burst {
        position: relative;
      }
      .price_burst > img {
        max-height: 420px;
        float: right;
      }
      .burst {
        position: absolute;
        top: 132px;
        right: 88px;
        display: flex;
        justify-content: center;
        align-items: center;
      }
      .burst img {
        max-height: 87px;
      }
      .burst span {
        position: absolute;
        color: white;
        font-size: 23px;
      }
      .order_btn {
        font-family: 'Lato';
        background: #009245;
        border: none;
        outline: none;
        font-weight: 600;
        font-size: 20px;
        margin: 1rem 0 1.5rem;
        padding: 10px 2rem;
        color: #fff;
      }
      .order_btn:hover {
        background: #014c25;
        color: #fff;
      }
      .order_btn:focus {
        outline: 0;
      }
   

      .picture {
        max-width: 600px;
        margin: 2rem auto !important;
        width: 100%;
      }

      .arrow_list li {
        padding-left: 5rem;
        position: relative;
        margin-bottom: 2rem;
      }

      .arrow_list li:before {
        content: "\f0a9";
        display: inline-block;
        font-family: FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        color: #c21b31;
        position: absolute;
        left: 1rem;
        font-size: 28px;
      }

      .checks_list li {
        padding-left: 5rem;
        position: relative;
        margin-bottom: 2rem;
      }

      .checks_list li:before {
        content: "\f046";
        display: inline-block;
        font-family: FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        color: #c21b31;
        position: absolute;
        left: 1rem;
        font-size: 28px;
      }

    

      .rep_box {
        padding: 6px 16px;
        background: #007518;
        color: #fff;
        display: inline-block;
        font-size: 18px;
        font-weight: 900;
        margin-bottom: 2rem;
        font-family: 'Lato';
      }

      .callout_box {
        padding: 5%;
        border: solid 4px #8a0d25;
      }

      .bonus_wrap {
        overflow: hidden;
        margin-top: 4rem;
        position: relative;
        display: flex;
        align-items: center;
      }

      .bonus_wrap > .report_side {
        flex: 0 0 auto;
        padding-left: 2rem;
      }

      .report_cover {
        max-height: 280px;
      }

      .report_cover_small {
        max-height: 240px;
      }

      .free_reports .title {
        font-size: 140%;
        font-weight: 600;
        font-family: 'Lato';
      }

      .free_reports_wrap {
          display: flex;
          align-items: center;
          margin-bottom: 4rem;
      }

      ul.rep_list li:before {
        content: "\f05d";
        display: inline-block;
        font-family: FontAwesome;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        
        position: absolute;
        font-size: 23px;
        left: -32px;
      }


    .free_reports_wrap .report_cover {
        max-height: 270px; 
    }

    .report_cover_top {
      max-height: 330px;
      transform: rotate(6deg);
    }

    .bundle ul { font-size: 18px; }
    #orderform > div > .bundle {
        padding-left: 6rem;
    }

        
      .customer.facing-title {
        display: none;
      }

      .underline {
        text-decoration: underline;
      }


  .bullets li {
    display: inline-block;
    list-style: none;
    margin: 0 0 16px 30px;
    padding: 0;
    position: relative;
}

.bullets li::before {
     color: #840C66;
    content: "\2022";
    display: inline-block;
    font-size: 2rem;
    left: -20px;
    position: absolute;
    top: 0;
    line-height: 0.8;
}


     

        .sec1_wrap {
          background: url(bg1.jpg);
          background-size: cover;
          background-repeat: no-repeat;
        }

     
        .section2, .section4 {
          background: url(block_bg_light_right.jpg)no-repeat top right;
          background-size: cover;

        } 

        .section1 .grad_bg {
          background: linear-gradient(90deg,rgba(15, 15, 15, 0),rgba(1, 2, 123, 0.7),rgba(0, 0, 0, 0));
          padding: 1rem 7%;
        }

        .section3 {
          background: url(block_bg_dark_left.jpg);
          background-size: cover;
          background-repeat: no-repeat;
        }

        .section5 {
          background: url(u3b2.png);
          background-size: contain;
          background-repeat: no-repeat;
          background-position: right;
        }

        .kari_img {
          width: 100%; 
          margin-top: -30px; 
          margin-bottom: -35px;
        }

        .down_img {
              width: 100%;
              margin-top: -38px;
              margin-bottom: -68px;
              max-width: 310px;
        }

        .testimonials {
            background: url(swirl.png) no-repeat center;
        }

        .cta_btn {
          background: linear-gradient(to bottom, #fbc357 0%, #ea8615 100%);
          border: 1px solid #D27812;
          border-radius: 5px;
          box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), inset 0 1px 2px rgba(255, 255, 255, 0.8);
          color: #333;
          display: inline-block;
          font-size: 20px;
          font-weight: 700;
          line-height: 1.2;
          margin-top: 14px;
          text-shadow: 0 1px 1px rgba(255, 255, 255, 0.4);
          text-decoration: none;
          text-transform: uppercase;
          width: 80%;
          max-width: 470px;
          padding: 20px 5px;
        }

        .cta_btn:hover {
          background: linear-gradient(to bottom, #fbda57 0%, #eaa215 100%);
          border: 1px solid #D28C12;
          color: #444;
        }
        
        .cta_btn_no {
          background: linear-gradient(to bottom, #F44336 0%, #F44336 100%);
          border: 1px solid #F44336;
          border-radius: 5px;
          box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), inset 0 1px 2px rgba(255, 255, 255, 0.8);
          color: #333;
          display: inline-block;
          font-size: 18px;
          font-weight: 500;
          height: 45px;
          line-height: 45px;
          margin-top: 14px;
          text-shadow: 0 1px 1px rgba(255, 255, 255, 0.4);
          text-decoration: none;
          text-transform: uppercase;
          width: 80%;
          max-width: 75px;
        }

        .cta_btn_no:hover {
          background: linear-gradient(to bottom, #f58e86 0%, #f58e86 100%);
          border: 1px solid #f58e86;
          color: #444;
        }

        .bonuses img{
          max-width: 250px;
        }

        .bonuses .bonus {
          font-size: 24px;
          display: block;
          color: #7D3B6B;
          margin-bottom: 0;
          font-family: 'Bree Serif', serif;
        }

        .bonuses .bonus_num {
          font-size: 100px;
          display: block;
          line-height: 80px;
          color: #7D3B6B;
          font-family: 'Bree Serif', serif;
        }

        .buy_box {
          border-radius: 10px;
          max-width: 600px;
          margin: 0 auto;
          background: linear-gradient(to bottom, #ffffff 0%, #c1c1c1 100%);
          text-align: center;
          overflow: hidden;
        }

        .buy_box_head {
          background: linear-gradient(to bottom, #882771 0%, #6f0d57 100%);
          padding: 1rem 2rem;
          border-top-left-radius: 10px;
          border-top-right-radius: 10px;
          font-family: 'Bree Serif', serif;
        }

        .secure-text {
          background: url(lock.png) no-repeat 26px center;
          font-size: 12px;
          margin: 0 0 14px;
          padding-left: 21px;
          display: inline-block;
          padding: 0 42px;
        }

        .order-info {
          font-size: 12px;
          line-height: 18px;
          margin: 28px 21px;
          padding-left: 105px;
          text-align: left;
          background: url('guarantee-1-year.png') no-repeat left center;
        }
 

        .custom_btn:hover {
          background: #d39a00;
        }

        @keyframes mymove {
          from { transform: scale(1) }
          to   { transform: scale(1.05) }
        }

        .custom_btn:focus {
          box-shadow: 0 0 0 11px rgba(244,187,32, 0.5);
        }

        .stop_header {
          padding: 1rem 0;
          border-bottom: solid 1px #ccc;
        }
        
        .prgrs {
           width: 550px;
           object-fit: cover;
        }

  
    @media (max-width: 767px) { 


        h1 {
          font-size: 2rem;
          line-height: 2.1rem;
        }

        .testimonail {
          height: inherit;
          margin-bottom: 2rem;
        }

        .custom_btn {
          box-shadow: none !important;
          margin: 4rem 2rem !important;          
        }      

        .row {
          margin-right: 0;
          margin-left: 0;
        }

        .kari_img {
          margin-top: 20px;
          margin-bottom: -25px;
        }

        .section5 {
          background: none;
        }

        .down_img {
           margin-bottom: -10px;
        }
        
        .prgrs {
           width: 200px;
           	
        }

        .cta_btn {
          font-size: 13px;
        }

        .cta_btn_no {
          font-size: 13px;
          height: 34px;
          max-width: 54px;
          line-height: 0;
        }

        .stop_header {
            padding: 1rem 0 2rem;
            border-bottom: solid 1px #ccc;
        }

        .is-current > .ProgressBar-stepLabel, .is-complete > .ProgressBar-stepLabel {
            color: #00ae37;
            font-size: 12px;
        }

  }

</style>

<!-- dots style -->

<style>
  
/*Downloaded from https://www.codeseek.co/BuddyLReno/dot-check-progress-bar-NNojRP */
.ProgressBar {
  margin: 0 auto;
  padding: 0em 0 2em;
  list-style: none;
  position: relative;
  display: flex;
  justify-content: space-between;
}

.ProgressBar-step {
  text-align: center;
  position: relative;
  width: 100%;
}
.ProgressBar-step:before, .ProgressBar-step:after {
  content: "";
  height: 0.5em;
  background-color: #9F9FA3;
  position: absolute;
  z-index: 1;
  width: 100%;
  left: -50%;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  transition: all .25s ease-out;
}
.ProgressBar-step:first-child:before, .ProgressBar-step:first-child:after {
  display: none;
}
.ProgressBar-step:after {
  background-color: #00ae37;
  width: 0%;
}
.ProgressBar-step.is-complete + .ProgressBar-step.is-current:after, .ProgressBar-step.is-complete + .ProgressBar-step.is-complete:after {
  width: 100%;
}

.ProgressBar-icon {
  width: 1.5em;
  height: 1.5em;
  background-color: #9F9FA3;
  fill: #9F9FA3;
  border-radius: 50%;
  padding: 0.5em;
  max-width: 100%;
  z-index: 10;
  position: relative;
  transition: all .25s ease-out;
}
.is-current .ProgressBar-icon {
  fill: #00ae37;
  background-color: #00ae37;
}
.is-complete .ProgressBar-icon {
  fill: #DBF1FF;
  background-color: #00ae37;
}

.ProgressBar-stepLabel {
  display: block;
  text-transform: uppercase;
  color: #9F9FA3;
  position: absolute;
  padding-top: 0.5em;
  width: 100%;
  transition: all .25s ease-out;
}
.is-current > .ProgressBar-stepLabel, .is-complete > .ProgressBar-stepLabel {
  color: #00ae37;
}

.wrapper {
  max-width: 1000px;
  margin: 0 auto;
  font-size: 16px;
}
</style>

<div class="stop_header">
<div class="container ">
  <div class="row justify-content-center">
    <div class="col-md-8 ">
    <img src="stop.png" class="img-fluid" style="max-width: 80px; width: 100%; float: left;">
    <div class="">
    <p class="text-center">¡Espera! Tu pedido no está completo todavía.</p>
    
      <div class="wrapper">     
          
        <ol class="ProgressBar">
          <li class="ProgressBar-step is-complete">
            <svg class="ProgressBar-icon"><use xlink:href="#checkmark-bold"/></svg>
            <span class="ProgressBar-stepLabel">Informe de lujo</span>
          </li>
          <li class="ProgressBar-step is-complete">
            <svg class="ProgressBar-icon"><use xlink:href="#checkmark-bold"/></svg>
            <span class="ProgressBar-stepLabel">Oferta especial</span>
          </li>
          <li class="ProgressBar-step is-current">
            <svg class="ProgressBar-icon"><use xlink:href="#checkmark-bold"/></svg>
            <span class="ProgressBar-stepLabel">orden completada</span>
          </li>        
        </ol>

      </div>

      <svg xmlns="http://www.w3.org/2000/svg" class="d-none">
        <symbol id="checkmark-bold" viewBox="0 0 24 24">
          <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/>
        </symbol>
      </svg>

      </div>
    </div>
  </div>
</div>
</div>

  

  

  <!-- <div class="container py-3">
    <img src="logo_color.png" class="img-fluid center" style="max-width: 280px; width: 100%">
  </div> -->



  <div class="sec1_wrap">
    <div class="section1 container">
      <div class="row">
        <div class="col-lg-8 offset-lg-2 py-5">

            <p class="font_115 text-center white_col">Hmm, nos dimos cuenta de algo fascinante en tu cuadro, (<b class='nd_name'>NAME</b> born <b class='nd_date'>DATE</b>)...</p>
             
             <h1 class="white_col text-center f600 my-4">Tu Número de Camino de Vida de "<b class='nd_num'></b>" Revela Un Bloqueo Subconsciente que Podría Evitar que desarrolles Tu Potencial Ilimitado en 2019</h1>


             <p class="white_col text-center f600 font_115 no_mg grad_bg">Luego de todo lo que has pasado el año pasado, odiaríamos ver que esto te detenga… </p>
        </div>
      </div>
    </div>
  </div>

  <div class=" container pb-5 px-0" style="border: solid 1px #ccc;">
    <div class="row section2 pt-5 mx-0">
      <div class="col-md-6">
        <h1 class="col_primary mg_bot2"><b class='nd_name'>NAME</b>, tu Número de Camino de Vida de "<b class='nd_num'>8</b>" te hace un tipo de persona responsable. </h1>
        <p>Eres apasionada, poderosa, magnética y puedes ver el panorama general de la vida. Eres una líder nata y tienes un carisma resaltante que puede llevarte lejos en cualquier empresa...</p>
        <p>Posees las cualidades perfectas para ser exitosa...</p>
        <p>Alguien que debería ser capaz de lograr la riqueza fácilmente y atraer oportunidades lucrativas únicas...</p>
        <p>De hecho, la abundancia es tu derecho de nacimiento. </p>
        <p>Pero cuando analicé tu nombre completo, el cual es <b class='nd_name'>NAME</b> y tu fecha de nacimiento que es el <b class='nd_date'>DATE</b>, me topé con algo que puede explicar la razón por la que todavía no has llegado hasta dónde quieres, al menos en términos financieros. </p>
        <p>Verás, a pesar de que tu numerología personal revela tus dotes, fortalezas y poderes innatos… también expone los patrones kármicos escondidos y las creencias subconscientes que sabotean silenciosamente tu éxito. Es la huella de tu alma y tiene una historia...</p>
        <p id='nd_num_base'>En base a tu Número de Camino de Vida de “<b class='nd_num'>8</b>” (y espero que no te lo tomes personal, <b class='nd_name'>Rbaxat</b>), puedo ver que a menudo desconfías de las personas con dinero, poder e influencia. Tiendes a disminuir tu deseo de éxito por miedo a que las personas te juzguen y te consideren tacaña o arrogante y a menudo estallas. No conoces tus límites y tratas de hacer todo por todo el mundo… </p>
        <p>Y lo creas o no… </p>
      </div>
      <div class="col-md-6">
        <img src="octagon_personalized_wealth-right-top.png" class="img-fluid d-none d-sm-block">
      </div>
    </div>  

    <div class="row">
      <div class="col-lg-8 offset-lg-2">
        <h1 class="col_primary my-5 text-center">Este Silencioso Bloqueo Subconsciente Te Está Haciendo Perder la Dirección de tu Vida y Está Deteniendo la Abundancia. </h1>
        <p class="f600">Como resultado...</p>
        <ul class="bullets pl-0">
          <li>Trabajas mucho pero parece que nunca obtienes tu recompensa...</li>
          <li>Te sientes incómoda cada vez que alguien habla sobre dinero o su éxito...</li>
          <li>Te cansas, frustrar y raramente eres reconocida por tu duro trabajo...</li>
          <li>Experimentas culpa o vergüenza por la cantidad de dinero que ganas...</li>
          <li>Y estás cansada de ver a todos los demás acumular dinero mientras tú lo pierdes.</li>
        </ul>
    </div> 
  </div>

  <?php echo "<p onclick='scroll_to_order()' id='astsal1' class='cta_btn center' style='cursor: pointer; display: block; text-align: center; margin-bottom: 3rem;'>¡Sí! ¡Quiero despejar mi bloqueo subconsciente y alcanzar mi potencial completo!</a>"; ?>

  <div class="section3 container py-5 px-0">
    <div class="row">
      <div class="col-md-7">
      <img src="darktree.png" class="img-fluid" style="width: 100%">
      </div>
      <div class="col-md-5 white_col ">
        <div class="pr-5">
          <p><b>Pero no dejes que esto te perturbe NAME</b></p>
          <p>La verdad es que TODO EL MUNDO sin importar sus orígenes, educación o situación financiera actual tiene su colección única de patrones <b>que evitan que desarrollen todo su potencial</b>. </p>
          <p>Incluso podrías haber heredado esos patrones de tus padres, o quizás los aprendiste en la escuela, o incluso los asimilaste en una vida pasada...</p>
          <p>Y sin importar lo duro que trabajes ni cuántos rasgos de éxito tengas, sin importar tu nivel de determinación o la fortaleza de tu carácter, nunca tendrás el aumento de abundancia que ansías hasta que elimines esos bloqueos subconscientes de una vez por todas. </p>
          <p>En el webcast de Kari Samuel llamado Activating Abundance (Activando la Abundancia), ella te muestra un poderoso ritual para aclarar las energías para remover los bloqueos más comunes que la persona enfrenta y, como resultado, verás una mejora inmediata en tus circunstancias financieras. </p>
          <p>Sin embargo, es increíblemente posible que tengas <b>otros bloqueos de abundancia que no sean tan fáciles de eliminar...</b> y quizás muy personales para cubrir en un webcast corto y público. </p>
          <p>Han hecho su camino desde tu distante pasado y en la mayoría de los casos ni siquiera sabes que están allí. Así que verás...</p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-8 offset-lg-2">
      <h1 class="text-center my-5 white_col">Tu Huella Numerológica Sólo Revela los Síntomas de Un Problema Mucho más Grande y Profundo...</h1>
      </div>
    </div>
  </div>

  <div class="section4 container py-5">
    <div class="row">
      <div class="col-lg-8 offset-lg-2 py-4">
        <p>Una cosa que la mayoría de los curanderos espirituales y coaches intuitivos simplemente no entienden porque <b>no puede alcanzarse a nivel físico, emocional o mental</b>. </p>
        <p>Esto requiere un esclarecimiento más profundo que trascienda las dimensiones del tiempo y del espacio...</p>
        <p>Y que realmente sane el karma de tu vida pasada a nivel espiritual para que puedas <b>crear nuevos patrones y restaurar tu abundancia innata...</b></p>
        <p>A diferencia de muchas técnicas superficiales enseñadas hoy en día que producen resultados superficiales que se desvanecen. </p>
        <p>Es por eso que nos complace presentarte la audio-clase de Kari Sameuls: Soul of Wealth </p>

        
      </div>
    </div>

    <div class="row">
      <div class="col-lg-10 offset-lg-1">
      <h1 class="text-center my-5 col_primary">Un Ritual Guiado para Limpiar las Energías y Sanar Los bloqueos de la Vida Pasada y Activar la Abundancia Espiritual </h1>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row" style="background: #000;">
      <div class="col-md-3">
        <img src="kari_smaller.png" class="img-fluid kari_img" >
      </div>
      <div class="col-md-9">
        <p class="white_col pr-5" style="padding-top: 2rem;">Con sus años de experiencia en astrología, numerología y asesoramiento intuitivo, y sanación, el don de Kari Samuels es combinar distintas formas de adivinación para transformar profundamente las vidas de sus estudiantes. Kari ha estado haciendo lecturas psíquicas y limpiezas espirituales durante más de 20 años y ha ayudado a miles de personas de todo el mundo a <b>crear un alma de riqueza y lograr la vida de sus sueños</b> por medio de sus poderosas técnicas y rituales. </p>
      </div>
    </div>

    <div class="row section5 mb-5">
      <div class="col-md-6 pt-5">
        <p>Una vez que experimentes esta limpieza espiritual, te sorprenderá lo rápido que mejorará tu vida. No sólo creará oportunidades para la abundancia en tu vida financiera- sino que también te sentirás más rico en <b>TODAS las áreas de tu vida</b> incluyendo el amor, las relaciones, la creatividad y la alegría. </p>
        <p><b>En esta clase guiada de 90 minutos tú:</b></p>
        <ul class="bullets pl-0">
          <li>Te liberarás los pactos de pobreza de la vida pasada y los traumas sin resolver que han estado evitando que prosperes y tengas éxito.</li>
          <li>Crearás nuevos patrones subconscientes que te permitirán lograr la abundancia con facilidad...</li>
          <li>Sanarás los sentimientos de vergüenza, culpa y estrés por dinero de modo que podrás permanecer en "atracción positiva"...</li>
          <li>Liberarás los pactos de pobreza de la vida pasada que han estado evitando que seas próspero y exitoso...</li>
          <li>Recibirás guía y apoyo de parte de alguien que entiende la profundidad de tu alma y de tu destino numerológico...</li>
          <li>Experimentarás un ritual de limpieza espiritual más poderoso y efectivo que cualquier otra cosa que hayas experimentado antes- Te lo garantizamos.</li>
        </ul>
      </div>
      <div class="col-md-6">
      </div>
    </div>

    <div class="row mb-5" style="background: #000;">
      <div class="col-md-4 offset-md-1">
        <img src="SOW-MAIN.png" class="img-fluid down_img" >
      </div>
      <div class="col-md-7">
        <p class="white_col pr-5" style="padding-top: 2rem;">Descarga esta audio-clase guiada para tu iPod, computadora o cualquier dispositivo mp3 y escúchala tanto como lo necesites. Te recomendamos hacerlo una vez por la mañana y una vez por la noche antes de dormir, hasta que sientas que el Alma de Riqueza empieza emerger. Sucederá más rápido de lo que crees. Tan sólo dale un vistazo a los resultados que estos estudiantes tuvieron con escucharla UNA vez.  </p>
      </div>
    </div>

    <div class="text-center mt-5 pt-4">
    <?php echo "<p onclick='scroll_to_order()' id='astsal1' class='cta_btn center' style='cursor: pointer'>¡Sí! ¡Quiero despejar mi bloqueo subconsciente y alcanzar mi potencial completo!</a>"; ?>
    </div>

    <div class="row testimonials">
      <div class="col-md-10 offset-md-1 pt-5">

        <div class="row mg_bot2">
          <div class="col-md-auto text-center">
            <img src="lpera.png" class="img-fluid mb-3 md-mb-0">
          </div>
          <div class="col">
            <p class="f600 font_115 text-md-left text-center">Lindsay Pera<br>San Louis Obispo, CA</p>
            <p class="text-md-left text-center">Oficialmente me he vuelto una seguidora de Kari Samuels. Esta clase no sólo iluminó mis bloqueos, sino que realmente me ayudó a borrar patrones que han estado retrasándome por años. No puedo creer lo equilibrada, inspirada y ligera que me sentí. Verdaderamente es un don divino del corazón- ¡No puedo agradecerle lo suficiente a Kari! </p>
          </div>
        </div>
        
        <div class="row mg_bot2">
          <div class="col-md-auto order-md-2 text-center">
            <img src="tpatillo.png" class="img-fluid mb-3 md-mb-0">
          </div>
          <div class="col order-md-1">
            <p class="f600 font_115 text-md-right text-center">Torrie Pattillo<br>Pikesville, MD</p>
            <p class="text-md-right text-center">Kari lo ha hecho otra vez: ¡abrió mi alma! Quería identificar el patrón financiero de mi alma, pero por supuesto que aprendí mucho más. Amo la manera en que cierra con un ejercicio de “limpieza” que llega a lo profundo y rompe el patrón negativo de una vez por todas.</p>
          </div>
        </div>
        
        <div class="row mg_bot2">
          <div class="col-md-auto text-center">
            <img src="roberts.png" class="img-fluid mb-3 md-mb-0">
          </div>
          <div class="col">
            <p class="f600 font_115 text-md-left text-center">Robert S.<br>San Diego, CA</p>
            <p class="text-md-left text-center">Recientemente tomé la clase de Kari del Alma de Riqueza. ¡Mis ingresos subieron 33% desde entonces! Kari explicó la manifestación en inglés sencillo. Dio ejemplos sencillos de entender que se adecuan a situaciones de la vida real. Mi parte favorita fue la sesión de sanación al final. ¡La sanación es algo poderoso! </p>
          </div>
        </div>

         
        <div class="row">
          <div class="col-md-auto order-md-2 text-center">
            <img src="rlee.png" class="img-fluid mb-3 md-mb-0">
          </div>
          <div class="col order-md-1">
            <p class="f600 font_115 text-md-right text-center">Rhonda Lee<br>SCarolina del Norte</p>
            <p class="text-md-right text-center">No puedo decir todas las cosas buenas que Kari Samules y su clase del Alma de Riqueza tienen. Literalmente volé cuando Kari habló sobre los pensamientos personales que han afectado el flujo de mi riqueza. Tuve muchos escalofríos en la clase. Meses después, aún estoy cosechando los beneficios. </p>
          </div>
        </div>

      </div>
    </div>

    <div class="row">
      <div class="col-md-10 offset-md-1 pt-5">
        <h1 class="col_primary mb-5 my-3 text-center">Un 20% de Descuentos y 5 Razones Más para Sonreír</h1>
        <p>Cuando ordenes la audi-clase del Alma de Riqueza, en esta página, ¡obtendrás un <b>20% de descuento MÁS 4 bonos especiales</b> creados por Kari Samuels! </p>
      </div>
    </div>


    <div class="row bonuses">
      <div class="col-md-10 offset-md-1 pt-5">

        <div class="row mg_bot2">
          <div class="col-md-2"><p class="text-center bonus">BONUS</p><p class="text-center bonus_num">1</p></div>
          <div class="col-md-auto text-center">
            <img src="SOW-BONUS-life-path-abundance-affirmations.png" class="img-fluid mb-3 md-mb-0">
          </div>
          <div class="col">
            <p>Afirmaciones para un Camino de Vida de Abundancia</p>
            <p>Tienes un perfil numerológico único, lo que significa que posees ciertos dones, fortalezas y debilidades que otros no. Entonces, ¿por qué usar un “enfoque único” para lograr la abundancia? En esta guía encontrarás afirmaciones unidas a TU Número de Camino de Vida Específico. Recítalas en la mañana y de nuevo antes de irte a la cama para un aumento de abundancia que dure todo el día. </p>
          </div>
        </div>
        
        <div class="row mg_bot2">
          <div class="col-md-2"><p class="text-center bonus">BONUS</p><p class="text-center bonus_num">2</p></div>
          <div class="col-md-auto order-md-2 text-center">
            <img src="SOW-BONUS-personal-year-numerology-guide.png" class="img-fluid mb-3 md-mb-0">
          </div>
          <div class="col order-md-1">
            <p>Guía de Numerología de Año Personal </p>
            <p>¿Buscas información sobre los retos y oportunidades que te esperan más adelante? Esta guía resulta bastante útil, y te enseñará a calcular e interpretar tu Número de Año Personal, ¡y así podrás planificarte para ser más exitoso y tener menos estrés! ¡Usa esta guía para darle a tus amigos y familiares lecturas personalizadas! </p>
          </div>
        </div>
        
        <div class="row mg_bot2">
          <div class="col-md-2"><p class="text-center bonus">BONUS</p><p class="text-center bonus_num">3</p></div>
          <div class="col-md-auto text-center">
            <img src="SOW-BONUS-personal-month-numerology-guide.png" class="img-fluid mb-3 md-mb-0">
          </div>
          <div class="col">
            <p>Guía de Numerología del Mes Personal </p>
            <p>Dentro de tus ciclos anules personales hay ciclos más cortos que cambian con cada mes del calendario en una sincronía divina. Bien sea que sea el momento de actuar, descansar, crear o intuir, cuando puedes aprovechar estas energías siempre estarás fluyendo en tu vida. Esta guía te ayudará a tomar decisiones a corto plazo de modo que puedas evitar obstáculos y crear oportunidades todos los días. </p>
          </div>
        </div>

         
        <div class="row">
          <div class="col-md-2"><p class="text-center bonus">BONUS</p><p class="text-center bonus_num">4</p></div>
          <div class="col-md-auto order-md-2 text-center">
            <img src="SOW-BONUS-envision-your-future.png" class="img-fluid mb-3 md-mb-0">
          </div>
          <div class="col order-md-1">
            <p>Visualiza tu Futuro: Meditación Guiada (MP3) </p>
            <p>Haz un viaje a los reinos del futuro y crea tu realidad ideal. En esta meditación guiada, Kari te enseñará a visualizar conscientemente tu vida deseada para que puedas lograr rápidamente y sin esfuerzo todos los deseos de tu corazón en este año y los años venideros. ¡Te sorprenderá ver tus sueños volverse realidad!  </p>
          </div>
        </div>

      </div>
    </div>

    <div class="row">
      <div class="col-md-10 offset-md-1">
        <h2 class="col_primary text-center my-5">Todos estos bonus GRATIS serán tuyos una vez que añadas la audio-clase titulada Alma de Riqueza (Soul of Wealth) de Kari Samules a tu pedido.</h2>

        <div class="buy_box">
          <div class="buy_box_head text-center white_col mb-4 font_115">
            ¡Haz clic en el botón y Paga tan sólo <?php echo do_shortcode("[wfocu_product_price_full key='18423fd12c3a8cf68361abd9c7cd552e']"); ?> Por la Audio-Clase del Alma de Riqueza y los 5 Bonus Especiales! 
          </div>
          <div id='down_order' name='down_order'><?php echo "<a href='javascript:void(0);' data-key='18423fd12c3a8cf68361abd9c7cd552e' class='wfocu_upsell wfocu-button wfocu-accept-button wfocu-btn-full wfocu-icon-hide no-effect'><button type='submit' id='astsal' class='cta_btn center' >¡Sí! ¡Quiero despejar mi bloqueo subconsciente y alcanzar mi potencial completo!</button></a>"; ?><br> <?php echo "<a href='javascript:void(0);' data-key='18423fd12c3a8cf68361abd9c7cd552e' class='wfocu_skip_offer wfocu-button wfocu-skip-button wfocu-btn-full wfocu-icon-hide no-effect' style='margin:0 auto;'><button type='submit' id='astsal_no' class='cta_btn_no center' >NO</button></a>"; ?></div>
          <p class="secure-text text-center">Todos los pedidos serán procesados en un servidor seguro </p>
          <p class="order-info" style="">Haz clic en el mencionado botón para añadir la audio-clase Alma de Riqueza a tu pedido. Sé que te encantará este programa, pero si por alguna razón no estás 100% satisfecho, simplemente envíanos un correo electrónico a: <a href="mailto:apoyo@numerosdevida.com" target="_blank" class="col_primary">apoyo@numerosdevida.com</a> en los 30 días posteriores a la fecha de tu compra y te devolveremos el dinero sin pregunta alguna. </p>
        </div>

      </div>
    </div>


  </div>
  </div>

  <div class="text-center my-4 " style="font-size: 12px;">
    <p>Copyright © 2019 numerosdevida.com. Todos los Derechos Reservados. </p>
    <p><a href="#">Política de Privacidad</a> | <a href="#">Términos de Uso</a> </p>
  </div>
  
  <?php //echo do_shortcode("[wfocu_product_regular_price key='18423fd12c3a8cf68361abd9c7cd552e']"); ?>





  




  </body>

</html>